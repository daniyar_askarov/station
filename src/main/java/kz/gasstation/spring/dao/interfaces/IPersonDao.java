package kz.gasstation.spring.dao.interfaces;

import kz.gasstation.spring.dao.entities.Person;

import java.util.List;

public interface IPersonDao {

    Person getPersonByLoginAndPassword(String login, String password);
    List<Person> getPersonsList();

}
