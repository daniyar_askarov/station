package kz.gasstation.spring.dao.serialize;

import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.databind.SerializerProvider;
import com.fasterxml.jackson.databind.ser.std.StdSerializer;
import kz.gasstation.spring.dao.entities.Gas;
import kz.gasstation.spring.dao.entities.Person;

import java.io.IOException;

public class CashierAuthSerialize extends StdSerializer<Person> {

    public CashierAuthSerialize(){
        this(null);
    }

    protected CashierAuthSerialize(Class<Person> t) {
        super(t);
    }

    @Override
    public void serialize(Person person, JsonGenerator jsonGenerator, SerializerProvider serializerProvider) throws IOException {
        jsonGenerator.writeStartObject();
        jsonGenerator.writeNumberField("CashierId", person.getPersonId());
        if(person.getShifts().size() != 0)
            jsonGenerator.writeNumberField("ShiftId", person.getShifts().get(0).getShiftId());
        jsonGenerator.writeArrayFieldStart("Gases");
        for (Gas gas: person.getDepartmentId().getGases()) {
            jsonGenerator.writeStartObject();
            jsonGenerator.writeNumberField("Id", gas.getGasId());
            jsonGenerator.writeStringField("Name", gas.getName());
            jsonGenerator.writeNumberField("Price", gas.getPrice());
            jsonGenerator.writeEndObject();
        }
        jsonGenerator.writeEndArray();
        jsonGenerator.writeEndObject();
    }
}
