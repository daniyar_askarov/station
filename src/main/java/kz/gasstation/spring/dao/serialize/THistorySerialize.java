package kz.gasstation.spring.dao.serialize;

import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.databind.SerializerProvider;
import com.fasterxml.jackson.databind.ser.std.StdSerializer;
import kz.gasstation.spring.dao.entities.TransactionHistory;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.List;

public class THistorySerialize extends StdSerializer<List<TransactionHistory>> {

    public THistorySerialize(){this(null);}

    public THistorySerialize(Class<List<TransactionHistory>> t) {
        super(t);
    }

    public void serialize(List<TransactionHistory> transactionHistories, JsonGenerator jsonGenerator, SerializerProvider serializerProvider) throws IOException {
        jsonGenerator.writeStartObject("Transactions");
        if(transactionHistories.get(transactionHistories.size()-1)!=null){
            TransactionHistory transactionHistory = transactionHistories.get(transactionHistories.size()-1);
            jsonGenerator.writeNumberField("NumberOfTransactions",transactionHistory.getPrice());
            transactionHistories.remove(transactionHistory);
        }
        jsonGenerator.writeArrayFieldStart("Transactions");
        for (TransactionHistory transactionHistory: transactionHistories) {
            jsonGenerator.writeStartObject();
            jsonGenerator.writeNumberField("Id", transactionHistory.getTransactionId());
            SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm");
            String date = dateFormat.format(transactionHistory.getDate());
            jsonGenerator.writeStringField("Date", date);
            jsonGenerator.writeNumberField("Price", transactionHistory.getPrice());
            jsonGenerator.writeStringField("Balance", transactionHistory.getBalance());
            jsonGenerator.writeStringField("Gas", transactionHistory.getGas());
            jsonGenerator.writeStringField("DepartmentName", transactionHistory.getDepartmentName());
            jsonGenerator.writeBooleanField("Refund", transactionHistory.isRefund());
            jsonGenerator.writeEndObject();
        }
        jsonGenerator.writeEndArray();
        jsonGenerator.writeEndObject();
    }
}
