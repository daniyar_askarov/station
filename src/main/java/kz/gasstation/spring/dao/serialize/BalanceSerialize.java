package kz.gasstation.spring.dao.serialize;

import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.databind.SerializerProvider;
import com.fasterxml.jackson.databind.ser.std.StdSerializer;
import kz.gasstation.spring.dao.entities.Account;
import kz.gasstation.spring.dao.entities.Person;

import java.io.IOException;
import java.util.List;

public class BalanceSerialize extends StdSerializer<Person> {

    public BalanceSerialize(){
        this(null);
    }

    public BalanceSerialize(Class<Person> t) {
        super(t);
    }

    public void serialize(Person person, JsonGenerator jsonGenerator, SerializerProvider serializerProvider) throws IOException {
        jsonGenerator.writeStartObject("Balance");
        List<Account> accounts = person.getAccounts();
        Account personalAccount = null;
        Account coperateAccount = null;
        for (Account account: accounts) {
            if(account.getName().equals("Personal")) personalAccount = account; else
                coperateAccount = account;
        }
        jsonGenerator.writeNumberField("PersonalBalance", personalAccount.getBalance());
        if(coperateAccount != null) {
            jsonGenerator.writeNumberField("CoperateBalance", coperateAccount.getBalance());
        }
        jsonGenerator.writeStringField("Role", person.getRole().getRole());
        jsonGenerator.writeEndObject();
    }
}
