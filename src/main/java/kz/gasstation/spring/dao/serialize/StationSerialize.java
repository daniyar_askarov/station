package kz.gasstation.spring.dao.serialize;

import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.databind.SerializerProvider;
import com.fasterxml.jackson.databind.ser.std.StdSerializer;
import kz.gasstation.spring.dao.entities.Account;
import kz.gasstation.spring.dao.entities.Department;
import kz.gasstation.spring.dao.entities.Person;

import java.io.IOException;

public class StationSerialize extends StdSerializer<Person> {

    public StationSerialize(){
        this(null);
    }

    protected StationSerialize(Class<Person> t) {
        super(t);
    }


    @Override
    public void serialize(Person person, JsonGenerator jsonGenerator, SerializerProvider serializerProvider) throws IOException {
        jsonGenerator.writeStartObject("Info");
        jsonGenerator.writeNumberField("CompanyId", person.getCompany().getCompanyId());
        jsonGenerator.writeStringField("CompanyName", person.getCompany().getName());
        jsonGenerator.writeStringField("Role", person.getRole().getRole());
        jsonGenerator.writeArrayFieldStart("Departments");
        for (Department department: person.getCompany().getDepartmentList()) {
            jsonGenerator.writeStartObject();
            jsonGenerator.writeNumberField("Id", department.getDepartmentId());
            jsonGenerator.writeStringField("Name", department.getName());
            jsonGenerator.writeNumberField("HeadId", department.getHead().getPersonId());
            jsonGenerator.writeStringField("HeadName", department.getHead().getName());
            jsonGenerator.writeStringField("HeadSurname", department.getHead().getSurname());
            jsonGenerator.writeEndObject();
        }
        jsonGenerator.writeEndArray();
        jsonGenerator.writeEndObject();
    }
}
