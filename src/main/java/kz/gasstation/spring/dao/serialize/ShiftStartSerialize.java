package kz.gasstation.spring.dao.serialize;

import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.databind.SerializerProvider;
import com.fasterxml.jackson.databind.ser.std.StdSerializer;
import kz.gasstation.spring.dao.entities.Person;
import kz.gasstation.spring.dao.entities.Shift;

import java.io.IOException;
import java.text.SimpleDateFormat;

public class ShiftStartSerialize extends StdSerializer<Shift>{

    public ShiftStartSerialize(){
        this(null);
    }

    public ShiftStartSerialize(Class<Shift> t) {
        super(t);
    }

    @Override
    public void serialize(Shift shift, JsonGenerator jsonGenerator, SerializerProvider serializerProvider) throws IOException {
        jsonGenerator.writeStartObject();
        jsonGenerator.writeNumberField("ShiftId", shift.getShiftId());
        jsonGenerator.writeEndObject();
    }
}
