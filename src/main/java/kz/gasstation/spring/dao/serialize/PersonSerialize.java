package kz.gasstation.spring.dao.serialize;

import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.SerializerProvider;
import com.fasterxml.jackson.databind.ser.std.StdSerializer;
import kz.gasstation.spring.dao.entities.Person;

import java.io.IOException;
import java.text.SimpleDateFormat;

public class PersonSerialize extends StdSerializer<Person> {

    public PersonSerialize(){
        this(null);
    }

    public PersonSerialize(Class<Person> p) {
        super(p);
    }

    public void serialize(Person person, JsonGenerator jsonGenerator, SerializerProvider serializerProvider) throws IOException, JsonProcessingException {
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-dd-MM");
        String birthdate = dateFormat.format(person.getBirthdate());
        jsonGenerator.writeStartObject("PersonInfo");
        jsonGenerator.writeNumberField("Id", person.getPersonId());
        jsonGenerator.writeStringField("Login", person.getLogin());
        jsonGenerator.writeStringField("Password", person.getPassword());
        jsonGenerator.writeStringField("Name", person.getName());
        jsonGenerator.writeStringField("Surname", person.getSurname());
        jsonGenerator.writeStringField("BirthDate", birthdate);
        jsonGenerator.writeStringField("Email", person.getEmail());
        jsonGenerator.writeStringField("Phone", person.getPhone());
        jsonGenerator.writeEndObject();
    }
}
