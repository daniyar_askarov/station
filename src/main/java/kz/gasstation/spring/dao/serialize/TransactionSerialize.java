package kz.gasstation.spring.dao.serialize;

import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.databind.SerializerProvider;
import com.fasterxml.jackson.databind.ser.std.StdSerializer;
import kz.gasstation.spring.dao.entities.TransactionHistory;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.List;

public class TransactionSerialize extends StdSerializer<List<TransactionHistory>> {

    public TransactionSerialize(){ this(null);}

    protected TransactionSerialize(Class<List<TransactionHistory>> t) {
        super(t);
    }

    @Override
    public void serialize(List<TransactionHistory> transactionHistories, JsonGenerator jsonGenerator, SerializerProvider serializerProvider) throws IOException {
        jsonGenerator.writeStartObject("Transactions");
        jsonGenerator.writeArrayFieldStart("Transactions");
        for (TransactionHistory transactionHistory: transactionHistories) {
            jsonGenerator.writeStartObject();
            jsonGenerator.writeNumberField("Id", transactionHistory.getTransactionId());
            SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm");
            String date = dateFormat.format(transactionHistory.getDate());
            jsonGenerator.writeStringField("Date", date);
            jsonGenerator.writeNumberField("Price", transactionHistory.getPrice());
            jsonGenerator.writeStringField("Balance", transactionHistory.getBalance());
            jsonGenerator.writeStringField("Gas", transactionHistory.getGas());
            jsonGenerator.writeStringField("Login", transactionHistory.getLogin());
            jsonGenerator.writeNumberField("Liters", transactionHistory.getPriceLiter());
            jsonGenerator.writeStringField("DepartmentName", transactionHistory.getDepartmentName());
            jsonGenerator.writeBooleanField("Refund", transactionHistory.isRefund());
            jsonGenerator.writeEndObject();
        }
        jsonGenerator.writeEndArray();
        jsonGenerator.writeEndObject();
    }
}
