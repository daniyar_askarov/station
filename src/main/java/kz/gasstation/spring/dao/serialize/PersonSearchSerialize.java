package kz.gasstation.spring.dao.serialize;

import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.databind.SerializerProvider;
import com.fasterxml.jackson.databind.ser.std.StdSerializer;
import kz.gasstation.spring.dao.entities.Person;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.List;

public class PersonSearchSerialize extends StdSerializer<List<Person>> {

    public PersonSearchSerialize(){
        this((Class<List<Person>>) null);
    }

    protected PersonSearchSerialize(Class<List<Person>> t) {
        super(t);
    }

    protected PersonSearchSerialize(StdSerializer<?> src) {
        super(src);
    }

    @Override
    public void serialize(List<Person> people, JsonGenerator jsonGenerator, SerializerProvider serializerProvider) throws IOException {
        jsonGenerator.writeStartObject("Result");
        jsonGenerator.writeArrayFieldStart("FoundedPersons");
        for (Person person: people) {
            SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-dd-MM");
            String birthdate = dateFormat.format(person.getBirthdate());
            jsonGenerator.writeStartObject("PersonInfo");
            jsonGenerator.writeNumberField("Id", person.getPersonId());
            jsonGenerator.writeStringField("Login", person.getLogin());
            jsonGenerator.writeStringField("Name", person.getName());
            jsonGenerator.writeStringField("Surname", person.getSurname());
            jsonGenerator.writeStringField("BirthDate", birthdate);
            jsonGenerator.writeStringField("Email", person.getEmail());
            jsonGenerator.writeStringField("Phone", person.getPhone());
            jsonGenerator.writeEndObject();
        }
        jsonGenerator.writeEndArray();
        jsonGenerator.writeEndObject();
    }


}
