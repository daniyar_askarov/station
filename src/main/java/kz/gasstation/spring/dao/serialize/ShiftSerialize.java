package kz.gasstation.spring.dao.serialize;

import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.databind.SerializerProvider;
import com.fasterxml.jackson.databind.ser.std.StdSerializer;
import kz.gasstation.spring.dao.entities.Shift;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.List;

public class ShiftSerialize extends StdSerializer<List<Shift>> {

    public ShiftSerialize(){
        this(null);
    }

    protected ShiftSerialize(Class<List<Shift>> t) {
        super(t);
    }

    @Override
    public void serialize(List<Shift> shifts, JsonGenerator jsonGenerator, SerializerProvider serializerProvider) throws IOException {
        jsonGenerator.writeStartObject("Shifts");
        if(shifts.get(shifts.size()-1)!=null){
            Shift shift = shifts.get(shifts.size()-1);
            jsonGenerator.writeNumberField("NumberOfShifts", shift.getProfit());
            shifts.remove(shift);
        }
        jsonGenerator.writeArrayFieldStart("Shifts");
        for (Shift shift: shifts) {
            jsonGenerator.writeStartObject();
            jsonGenerator.writeNumberField("Id", shift.getShiftId());
            jsonGenerator.writeStringField("Status", shift.getStatus());
            SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm");
            String date = dateFormat.format(shift.getStart());
            jsonGenerator.writeStringField("StartDate", date);
            if(shift.getEnd() != null){
                date = dateFormat.format(shift.getStart());
                jsonGenerator.writeStringField("EndDate", date);
            }
            jsonGenerator.writeNumberField("Profit", shift.getProfit());
            jsonGenerator.writeEndObject();
        }
        jsonGenerator.writeEndArray();
        jsonGenerator.writeEndObject();
    }
}
