package kz.gasstation.spring.dao.serialize;

import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.SerializerProvider;
import com.fasterxml.jackson.databind.ser.std.StdSerializer;
import kz.gasstation.spring.dao.entities.Department;
import kz.gasstation.spring.dao.entities.Person;

import java.io.IOException;

public class DepartmentSerialize extends StdSerializer<Department> {

    public DepartmentSerialize(){
        this(null);
    }

    public DepartmentSerialize(Class<Department> p) {
        super(p);
    }

    public void serialize(Department department, JsonGenerator jsonGenerator, SerializerProvider serializerProvider) throws IOException, JsonProcessingException {
        jsonGenerator.writeStartObject("DepartmentInfo");
        jsonGenerator.writeNumberField("Id", department.getDepartmentId());
        jsonGenerator.writeStringField("Name", department.getName());
        jsonGenerator.writeObjectFieldStart("HeadInfo");
        jsonGenerator.writeStringField("Name", department.getHead().getName());
        jsonGenerator.writeStringField("Surname", department.getHead().getSurname());
        jsonGenerator.writeEndObject();
        jsonGenerator.writeArrayFieldStart("personals");
        for (Person person : department.getPersonList()) {
            jsonGenerator.writeStartObject();
            jsonGenerator.writeNumberField("Id", person.getPersonId());
            jsonGenerator.writeStringField("Name", person.getName());
            jsonGenerator.writeStringField("Surname", person.getSurname());
            jsonGenerator.writeStringField("Email", person.getEmail());
            jsonGenerator.writeStringField("Phone", person.getPhone());
            jsonGenerator.writeEndObject();
        }
        jsonGenerator.writeEndArray();
        jsonGenerator.writeEndObject();
    }
}