package kz.gasstation.spring.dao.serialize;

import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.databind.SerializerProvider;
import com.fasterxml.jackson.databind.ser.std.StdSerializer;
import kz.gasstation.spring.dao.entities.Person;
import kz.gasstation.spring.dao.entities.TransactionHistory;

import java.io.IOException;

public class TotalInfoSerialize extends StdSerializer<Person> {

    public TotalInfoSerialize() {
        this(null);
    }

    public TotalInfoSerialize(Class<Person> t) {
        super(t);
    }

    public void serialize(Person person, JsonGenerator jsonGenerator, SerializerProvider serializerProvider) throws IOException {
        jsonGenerator.writeStartObject("PersonInfo");
        jsonGenerator.writeNumberField("Id", person.getPersonId());
        jsonGenerator.writeStringField("Login", person.getLogin());
        jsonGenerator.writeStringField("Password", person.getPassword());
        jsonGenerator.writeStringField("Name", person.getName());
        jsonGenerator.writeStringField("Surname", person.getSurname());
        jsonGenerator.writeStringField("Email", person.getEmail());
        jsonGenerator.writeStringField("Phone", person.getPhone());
        jsonGenerator.writeStringField("Birthdate", person.getBirthdate().toString());
        jsonGenerator.writeNumberField("PersonalBalance", person.getAccounts().get(0).getBalance());
        jsonGenerator.writeArrayFieldStart("Transactions");
        for (TransactionHistory transactionHistory: person.getTransactionHistories()) {
            jsonGenerator.writeStartObject();
            jsonGenerator.writeNumberField("Id", transactionHistory.getTransactionId());
            jsonGenerator.writeStringField("Date", transactionHistory.getDate().toString());
            jsonGenerator.writeNumberField("Price", transactionHistory.getPrice());
            jsonGenerator.writeStringField("DepartmentName", transactionHistory.getShiftId().getPersonId().getDepartmentId().getName());
            jsonGenerator.writeEndObject();
        }
        jsonGenerator.writeEndArray();
        if(person.getDepartment() != null){
            jsonGenerator.writeNumberField("CorporateBalance", person.getAccounts().get(1).getBalance());
            jsonGenerator.writeObjectFieldStart("DepartmentInfo");
            jsonGenerator.writeNumberField("Id", person.getDepartment().getDepartmentId());
            jsonGenerator.writeStringField("Name", person.getDepartment().getName());
            if(person.getPersonId() != person.getDepartment().getHead().getPersonId()) {
                jsonGenerator.writeStringField("HeadName", person.getDepartment().getHead().getName());
                jsonGenerator.writeStringField("HeadSurname", person.getDepartment().getHead().getSurname());
            }
            jsonGenerator.writeEndObject();
            jsonGenerator.writeObjectFieldStart("CompanyInfo");
            jsonGenerator.writeNumberField("Id", person.getDepartment().getCompany().getCompanyId());
            jsonGenerator.writeStringField("Name", person.getDepartment().getCompany().getName());
            if(person.getPersonId() != person.getDepartment().getCompany().getHead().getPersonId()) {
                jsonGenerator.writeStringField("HeadName", person.getDepartment().getCompany().getHead().getName());
                jsonGenerator.writeStringField("HeadSurname", person.getDepartment().getCompany().getHead().getSurname());
            }
            jsonGenerator.writeEndObject();
        }
        jsonGenerator.writeEndObject();
    }
}
