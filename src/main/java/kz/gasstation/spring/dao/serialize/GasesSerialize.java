package kz.gasstation.spring.dao.serialize;

import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.databind.SerializerProvider;
import com.fasterxml.jackson.databind.ser.std.StdSerializer;
import kz.gasstation.spring.dao.entities.Gas;

import java.io.IOException;
import java.util.List;

public class GasesSerialize extends StdSerializer<List<Gas>> {

    public GasesSerialize(){
        this(null);
    }

    protected GasesSerialize(Class<List<Gas>> t) {
        super(t);
    }

    @Override
    public void serialize(List<Gas> gasList, JsonGenerator jsonGenerator, SerializerProvider serializerProvider) throws IOException {
        jsonGenerator.writeStartObject("GasesInfo");
        jsonGenerator.writeArrayFieldStart("Gas");
        for (Gas gas : gasList) {
            jsonGenerator.writeStartObject();
            jsonGenerator.writeNumberField("Id", gas.getGasId());
            jsonGenerator.writeStringField("Name", gas.getName());
            jsonGenerator.writeNumberField("Price", gas.getPrice());
            jsonGenerator.writeEndObject();
        }
        jsonGenerator.writeEndArray();
        jsonGenerator.writeEndObject();
    }
}
