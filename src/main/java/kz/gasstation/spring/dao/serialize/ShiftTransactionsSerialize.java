package kz.gasstation.spring.dao.serialize;

import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.SerializerProvider;
import com.fasterxml.jackson.databind.deser.std.StdDeserializer;
import com.fasterxml.jackson.databind.ser.std.StdSerializer;
import kz.gasstation.spring.dao.entities.Shift;
import kz.gasstation.spring.dao.entities.TransactionHistory;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.List;

public class ShiftTransactionsSerialize extends StdSerializer<Shift> {

    public ShiftTransactionsSerialize(){
        this(null);
    }

    public ShiftTransactionsSerialize(Class<Shift> t) {
        super(t);
    }

    @Override
    public void serialize(Shift shift, JsonGenerator jsonGenerator, SerializerProvider serializerProvider) throws IOException {
        jsonGenerator.writeStartObject("ShiftInfo");
        jsonGenerator.writeNumberField("Id", shift.getShiftId());
        jsonGenerator.writeStringField("Status", shift.getStatus());
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm");
        String date = dateFormat.format(shift.getStart());
        jsonGenerator.writeStringField("StartDate", date);
        if(shift.getEnd() != null){
            date = dateFormat.format(shift.getStart());
            jsonGenerator.writeStringField("EndDate", date);
        }
        jsonGenerator.writeStringField("DepartmentName", shift.getPersonId().getDepartmentId().getName());
        jsonGenerator.writeNumberField("Profit", shift.getProfit());
        List<TransactionHistory> transactionHistories = shift.getTransactions();
        jsonGenerator.writeArrayFieldStart("Transactions");
        for (TransactionHistory transactionHistory: transactionHistories) {
            jsonGenerator.writeStartObject();
            jsonGenerator.writeNumberField("Id", transactionHistory.getTransactionId());
            date = dateFormat.format(transactionHistory.getDate());
            jsonGenerator.writeStringField("Date", date);
            jsonGenerator.writeNumberField("Price", transactionHistory.getPrice());
            jsonGenerator.writeStringField("Balance", transactionHistory.getBalance());
            jsonGenerator.writeStringField("Gas", transactionHistory.getGas());
            jsonGenerator.writeStringField("Login", transactionHistory.getLogin());
            jsonGenerator.writeNumberField("Liters", transactionHistory.getPriceLiter());
            jsonGenerator.writeBooleanField("Refund", transactionHistory.isRefund());
            jsonGenerator.writeEndObject();
        }
        jsonGenerator.writeEndArray();
        jsonGenerator.writeEndObject();
    }
}