package kz.gasstation.spring.dao.serialize;

import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.databind.SerializerProvider;
import com.fasterxml.jackson.databind.ser.std.StdSerializer;
import kz.gasstation.spring.dao.entities.Person;
import kz.gasstation.spring.dao.entities.TransactionHistory;

import java.io.IOException;
import java.text.SimpleDateFormat;

public class DashboardInfoSerialize extends StdSerializer<Person> {

    public DashboardInfoSerialize(){
        this(null);
    }

    public DashboardInfoSerialize(Class<Person> t) {
        super(t);
    }

    public void serialize(Person person, JsonGenerator jsonGenerator, SerializerProvider serializerProvider) throws IOException {
        jsonGenerator.writeStartObject("PersonInfo");
        jsonGenerator.writeNumberField("Id", person.getPersonId());
        jsonGenerator.writeStringField("Login", person.getLogin());
        jsonGenerator.writeStringField("Password", person.getPassword());
        jsonGenerator.writeStringField("Name", person.getName());
        jsonGenerator.writeStringField("Surname", person.getSurname());
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-dd-MM");
        String birthdate = dateFormat.format(person.getBirthdate());
        jsonGenerator.writeStringField("BirthDate", birthdate);
        jsonGenerator.writeStringField("Email", person.getEmail());
        jsonGenerator.writeStringField("Phone", person.getPhone());
        jsonGenerator.writeStringField("Role", person.getRole().getRole());
        jsonGenerator.writeNumberField("PersonalBalance", person.getAccounts().get(0).getBalance());
        jsonGenerator.writeStringField("ActiveBalance", person.getActiveAccount().getName());
        jsonGenerator.writeArrayFieldStart("Transactions");
        for (TransactionHistory transactionHistory: person.getTransactionHistories()) {
            jsonGenerator.writeStartObject();
            jsonGenerator.writeNumberField("Id", transactionHistory.getTransactionId());
            dateFormat = new SimpleDateFormat("yyyy-dd-MM HH:mm");
            String date = dateFormat.format(transactionHistory.getDate());
            jsonGenerator.writeStringField("Date", date);
            jsonGenerator.writeNumberField("Price", transactionHistory.getPrice());
            jsonGenerator.writeStringField("Name", transactionHistory.getShiftId().getPersonId().getDepartmentId().getName());
            jsonGenerator.writeEndObject();
        }
        jsonGenerator.writeEndArray();
        if(person.getDepartment() != null) {
            jsonGenerator.writeNumberField("CooperateBalance", person.getAccounts().get(1).getBalance());
        }
        jsonGenerator.writeEndObject();
    }
}
