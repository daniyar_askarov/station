package kz.gasstation.spring.dao.serialize;

import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.databind.SerializerProvider;
import com.fasterxml.jackson.databind.ser.std.StdSerializer;
import kz.gasstation.spring.dao.entities.Account;
import kz.gasstation.spring.dao.entities.Department;
import kz.gasstation.spring.dao.entities.Person;

import java.io.IOException;

public class B2BHeadSerialize extends StdSerializer<Person> {

    public B2BHeadSerialize(){
        this(null);
    }

    protected B2BHeadSerialize(Class<Person> t) {
        super(t);
    }

    @Override
    public void serialize(Person person, JsonGenerator jsonGenerator, SerializerProvider serializerProvider) throws IOException {
        jsonGenerator.writeStartObject("B2BInfo");
        jsonGenerator.writeNumberField("CompanyId", person.getCompany().getCompanyId());
        jsonGenerator.writeStringField("CompanyName", person.getCompany().getName());
        Department department = person.getDep();
        jsonGenerator.writeNumberField("DepartmentId", department.getDepartmentId());
        jsonGenerator.writeStringField("DepartmentName", department.getName());
        double departmentBalance = 0.0;
        for (Account account: person.getAccounts()) {
            if(account.getName().equals("Coperate")) departmentBalance = account.getBalance();
        }
        jsonGenerator.writeNumberField("DepartmentBalance", departmentBalance);
        jsonGenerator.writeNumberField("DepartmentHeadId", department.getHead().getPersonId());
        jsonGenerator.writeStringField("DepartmentHeadName", department.getHead().getName());
        jsonGenerator.writeStringField("DepartmentHeadSurname", department.getHead().getSurname());
        jsonGenerator.writeNumberField("DepartmentNumberOfStaff", department.getNumberOfstaff());
        jsonGenerator.writeArrayFieldStart("DepartmentStaff");
        for (Person persons: department.getPersonList()) {
            jsonGenerator.writeStartObject();
            jsonGenerator.writeNumberField("Id", persons.getPersonId());
            jsonGenerator.writeStringField("Name", persons.getName());
            jsonGenerator.writeStringField("Surname", persons.getSurname());
            double coperateBalance = 0.0;
            for (Account account: persons.getAccounts()) {
                if(account.getName().equals("Coperate")) coperateBalance = account.getBalance();
            }
            jsonGenerator.writeNumberField("Balance", coperateBalance);
            jsonGenerator.writeStringField("Phone", persons.getPhone());
            jsonGenerator.writeEndObject();
        }
        jsonGenerator.writeEndArray();
        jsonGenerator.writeEndObject();
    }
}
