package kz.gasstation.spring.dao.serialize;

import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.databind.SerializerProvider;
import com.fasterxml.jackson.databind.ser.std.StdSerializer;
import kz.gasstation.spring.dao.entities.Account;
import kz.gasstation.spring.dao.entities.Department;
import kz.gasstation.spring.dao.entities.Person;

import java.io.IOException;

public class B2BDirectorSerialize extends StdSerializer<Person> {

    public B2BDirectorSerialize(){
        this(null);
    }

    protected B2BDirectorSerialize(Class<Person> t) {
        super(t);
    }

    @Override
    public void serialize(Person person, JsonGenerator jsonGenerator, SerializerProvider serializerProvider) throws IOException {
        jsonGenerator.writeStartObject("B2BInfo");
        jsonGenerator.writeNumberField("CompanyId", person.getCompany().getCompanyId());
        jsonGenerator.writeStringField("CompanyName", person.getCompany().getName());
        double companyBalance = 0.0;
        for (Account account: person.getAccounts()) {
            if(account.getName().equals("Coperate")) companyBalance = account.getBalance();
        }
        jsonGenerator.writeNumberField("CompanyBalance", companyBalance);
        jsonGenerator.writeArrayFieldStart("Departments");
        for (Department department: person.getCompany().getDepartmentList()) {
            jsonGenerator.writeStartObject();
            jsonGenerator.writeNumberField("Id", department.getDepartmentId());
            jsonGenerator.writeStringField("Name", department.getName());
            jsonGenerator.writeNumberField("HeadId", department.getHead().getPersonId());
            jsonGenerator.writeStringField("HeadName", department.getHead().getName());
            jsonGenerator.writeStringField("HeadSurname", department.getHead().getSurname());
            jsonGenerator.writeStringField("HeadLogin", department.getHead().getLogin());
            double coperateBalance = 0.0;
            for (Account account:  department.getHead().getAccounts()) {
                if(account.getName().equals("Coperate")) coperateBalance = account.getBalance();
            }
            jsonGenerator.writeNumberField("DepartmentBalance", coperateBalance);
            jsonGenerator.writeEndObject();
        }
        jsonGenerator.writeEndArray();
        jsonGenerator.writeEndObject();
    }
}
