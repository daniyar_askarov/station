package kz.gasstation.spring.dao.serialize;

import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.databind.SerializerProvider;
import com.fasterxml.jackson.databind.ser.std.StdSerializer;
import kz.gasstation.spring.dao.entities.Company;
import kz.gasstation.spring.dao.entities.Gas;

import java.io.IOException;
import java.util.List;

public class CompanyProfitSerialize extends StdSerializer<List<Company>> {

    public CompanyProfitSerialize() { this(null); }

    protected CompanyProfitSerialize(Class<List<Company>> t) {
        super(t);
    }

    @Override
    public void serialize(List<Company> companies, JsonGenerator jsonGenerator, SerializerProvider serializerProvider) throws IOException {
        jsonGenerator.writeStartObject("CompaniesInfo");
        jsonGenerator.writeArrayFieldStart("Companies");
        for (Company company: companies) {
            jsonGenerator.writeStartObject();
            jsonGenerator.writeNumberField("Id", company.getCompanyId());
            jsonGenerator.writeStringField("Name", company.getName());
            jsonGenerator.writeNumberField("Profit", company.getHead().getPersonId());
            jsonGenerator.writeNumberField("Liters", company.getHead().getDepartment().getDepartmentId());
            jsonGenerator.writeEndObject();
        }
        jsonGenerator.writeEndArray();
        jsonGenerator.writeEndObject();
    }
}
