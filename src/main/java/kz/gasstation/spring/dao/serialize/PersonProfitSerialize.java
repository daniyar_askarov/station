package kz.gasstation.spring.dao.serialize;

import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.databind.SerializerProvider;
import com.fasterxml.jackson.databind.ser.std.StdSerializer;
import kz.gasstation.spring.dao.entities.Company;
import kz.gasstation.spring.dao.entities.Gas;
import kz.gasstation.spring.dao.entities.Person;
import kz.gasstation.spring.dao.entities.TransactionHistory;

import java.io.IOException;
import java.util.List;

public class PersonProfitSerialize extends StdSerializer<List<Person>> {

    public PersonProfitSerialize() {this(null);}

    protected PersonProfitSerialize(Class<List<Person>> t) {
        super(t);
    }

    @Override
    public void serialize(List<Person> people, JsonGenerator jsonGenerator, SerializerProvider serializerProvider) throws IOException {
        jsonGenerator.writeStartObject("PeopleInfo");
        jsonGenerator.writeArrayFieldStart("People");
        for (Person person: people) {
            jsonGenerator.writeStartObject();
            jsonGenerator.writeNumberField("Id", person.getPersonId());
            jsonGenerator.writeStringField("Name", person.getLogin());
            jsonGenerator.writeNumberField("Profit", person.getDepartment().getDepartmentId());
            jsonGenerator.writeNumberField("Liters", person.getDepartment().getHead().getPersonId());
            jsonGenerator.writeArrayFieldStart("Gases");
            for (TransactionHistory history: person.getTransactionHistories()) {
                jsonGenerator.writeStartObject();
                jsonGenerator.writeStringField("Name", history.getGas());
                jsonGenerator.writeNumberField("Profit", history.getPrice());
                jsonGenerator.writeNumberField("Liters", history.getPriceLiter());
                jsonGenerator.writeBooleanField("refund", history.isRefund());
                jsonGenerator.writeEndObject();
            }
            jsonGenerator.writeEndArray();
            jsonGenerator.writeEndObject();
        }
        jsonGenerator.writeEndArray();
        jsonGenerator.writeArrayFieldStart("Gases");
        /*for (Gas gas: people.get(0).getDepartment().getGases()) {
            jsonGenerator.writeStartObject();
            jsonGenerator.writeStringField("Name", gas.getName());
            jsonGenerator.writeNumberField("Profit", gas.getPrice());
            jsonGenerator.writeNumberField("Liters", gas.getGasId());
            jsonGenerator.writeEndObject();
        }*/
        jsonGenerator.writeEndArray();
        jsonGenerator.writeEndObject();
    }
}
