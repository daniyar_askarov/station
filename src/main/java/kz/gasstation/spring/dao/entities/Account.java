package kz.gasstation.spring.dao.entities;

import com.fasterxml.jackson.annotation.JsonRootName;

import javax.persistence.*;

@Entity
@Table(name = "account")
@JsonRootName("accountInfo")
public class Account {

    @Id
    @Column(name = "accountId")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long accountId;

    @Column(name = "name")
    private String name;

    @Column(name = "balance")
    private double balance;

    @OneToOne(mappedBy = "activeAccount")
    private Person person;

    @ManyToOne
    @JoinColumn(name = "personId",nullable = false)
    private Person personId;

    public Account() {
    }


    public Account(String name, double balance, Person personId) {
        this.name = name;
        this.balance = balance;
        this.personId = personId;
    }

    public Account(String name) {
        this.name = name;
    }

    public Account(String name, double balance, Person person, Person personId) {
        this.name = name;
        this.balance = balance;
        this.person = person;
        this.personId = personId;
    }

    public Account(String name, Person person) {
        this.name = name;
        this.personId = person;
    }

    public long getAccountId() {
        return accountId;
    }

    public void setAccountId(long accountId) {
        this.accountId = accountId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public double getBalance() {
        return balance;
    }

    public void setBalance(double balance) {
        this.balance = balance;
    }

    public Person getPersonId() {
        return personId;
    }

    public void setPersonId(Person personId) {
        this.personId = personId;
    }

    public Person getPerson() {
        return person;
    }

    public void setPerson(Person person) {
        this.person = person;
    }
}
