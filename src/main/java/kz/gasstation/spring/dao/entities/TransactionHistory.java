package kz.gasstation.spring.dao.entities;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonRootName;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import kz.gasstation.spring.dao.deserialize.PaymentDeserialize;
import kz.gasstation.spring.dao.serialize.THistorySerialize;

import javax.persistence.*;
import java.util.Date;

@Entity
@Table(name = "transaction")
@JsonRootName("transaction")
@JsonIgnoreProperties(ignoreUnknown = true)
public class TransactionHistory {

    @Id
    @Column(name = "transactionId")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long transactionId;

    @Column(name = "price")
    private double price;

    @Column(name = "liter", nullable = false)
    private double priceLiter;

    @Column(name = "date")
    private Date date;

    @Column(name = "balance")
    private String balance;

    @Column(name = "Gas")
    private String gas;

    @Column(name = "login")
    private String login;

    @Column(name = "refund")
    private boolean refund;

    @Column(name = "Department")
    private String departmentName;

    @ManyToOne
    @JoinColumn(name = "personId",nullable = false)
    private Person personId;

    @ManyToOne
    @JoinColumn(name = "shiftId",nullable = false)
    private Shift shiftId;

    public TransactionHistory(@JsonProperty("transactionId") long transactionId){
        this.transactionId = transactionId;
    }

    public TransactionHistory(long personId, long shiftId, long price, String activeAccountName, String gas){
        this.price = price;
        this.gas = gas;
        Person person = new Person();
        person.setPersonId(personId);
        Account account = new Account(activeAccountName);
        person.setActiveAccount(account);
        this.setPersonId(person);

        Shift shift = new Shift(shiftId);
        this.shiftId = shift;
    }

    public TransactionHistory(long personId, long shiftId, long price,long priceLiter,String activeAccountName, String gas){
        this.price = price;
        this.priceLiter = priceLiter;
        this.gas = gas;
        Person person = new Person();
        person.setPersonId(personId);
        Account account = new Account(activeAccountName);
        person.setActiveAccount(account);
        this.setPersonId(person);

        Shift shift = new Shift(shiftId);
        this.shiftId = shift;
    }

    public TransactionHistory() {
    }

    public TransactionHistory(String gas) {
        this.gas = gas;
    }

    public TransactionHistory(Date date) {
        this.date = date;
    }

    public TransactionHistory(double price, double priceLiter, Date date, String balance, Person personId, String gas, String login,
                                   Shift shift, String departmentName) {
        this.price = price;
        this.priceLiter = priceLiter;
        this.date = date;
        this.balance = balance;
        this.personId = personId;
        this.gas = gas;
        this.shiftId = shift;
        this.departmentName = departmentName;
        this.login = login;
    }

    public long getTransactionId() {
        return transactionId;
    }

    public void setTransactionId(long transactionId) {
        this.transactionId = transactionId;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public Person getPersonId() {
        return personId;
    }

    public void setPersonId(Person personId) {
        this.personId = personId;
    }

    public Shift getShiftId() {
        return shiftId;
    }

    public void setShiftId(Shift shiftId) {
        this.shiftId = shiftId;
    }

    public String getBalance() {
        return balance;
    }

    public void setBalance(String balance) {
        this.balance = balance;
    }

    public String getGas() {
        return gas;
    }

    public void setGas(String gas) {
        this.gas = gas;
    }

    public String getDepartmentName() {
        return departmentName;
    }

    public void setDepartmentName(String departmentName) {
        this.departmentName = departmentName;
    }

    public double getPriceLiter() {
        return priceLiter;
    }

    public void setPriceLiter(double priceLiter) {
        this.priceLiter = priceLiter;
    }

    public String getLogin() {
        return login;
    }

    public void setLogin(String login) {
        this.login = login;
    }

    public boolean isRefund() {
        return refund;
    }

    public void setRefund(boolean refund) {
        this.refund = refund;
    }
}
