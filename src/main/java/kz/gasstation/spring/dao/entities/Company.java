package kz.gasstation.spring.dao.entities;

import com.fasterxml.jackson.annotation.JsonGetter;
import com.fasterxml.jackson.annotation.JsonRootName;
import org.hibernate.annotations.Fetch;
import org.hibernate.annotations.FetchMode;

import javax.persistence.*;
import java.util.List;

@Entity
@Table(name = "company")
@JsonRootName("companyInfo")
public class Company {

    @Id
    @Column(name = "companyId")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long companyId;

    @Column(name = "name")
    private String name;

    @OneToOne(fetch = FetchType.EAGER,cascade = CascadeType.ALL)
    @JoinColumn(name = "head",nullable = false)
    private Person head;

    @OneToMany(fetch = FetchType.EAGER,mappedBy = "company",cascade = CascadeType.ALL)
    @Fetch(value = FetchMode.SUBSELECT)
    private List<Department> departmentList;

    public Company(String name, Person head, List<Department> departmentList) {
        this.name = name;
        this.head = head;
        this.departmentList = departmentList;
    }

    public Company() {
    }

    public Company(long companyId) {
        this.companyId = companyId;
    }

    @JsonGetter(value = "company-id")
    public long getCompanyId() {
        return companyId;
    }

    public void setCompanyId(long companyId) {
        this.companyId = companyId;
    }

    @JsonGetter(value = "name")
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @JsonGetter(value = "head")
    public Person getHead() {
        return head;
    }

    public void setHead(Person head) {
        this.head = head;
    }

    @JsonGetter(value = "department-list")
    public List<Department> getDepartmentList() {
        return departmentList;
    }

    public void setDepartmentList(List<Department> departmentList) {
        this.departmentList = departmentList;
    }

}
