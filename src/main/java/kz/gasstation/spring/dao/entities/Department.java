package kz.gasstation.spring.dao.entities;

import org.hibernate.annotations.Fetch;
import org.hibernate.annotations.FetchMode;

import javax.persistence.*;
import java.util.List;

@Entity
@Table(name = "department")
public class Department {

    @Id
    @Column(name = "departmentId")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long departmentId;

    @Column(name = "name")
    private String name;

    @OneToOne(fetch = FetchType.EAGER,cascade = CascadeType.MERGE)
    @JoinColumn(name = "head",nullable = true)
    private Person head;

    @OneToMany(fetch = FetchType.EAGER,mappedBy = "departmentId",cascade = CascadeType.MERGE)
    @Fetch(value = FetchMode.JOIN)
    private List<Person> personList;

    @OneToMany(fetch = FetchType.EAGER,mappedBy = "department",cascade = CascadeType.MERGE)
    @Fetch(value = FetchMode.JOIN)
    private List<Gas> gases;

    private long numberOfstaff;

    @ManyToOne
    @JoinColumn(name = "companyId",nullable = false)
    private Company company;

    public Department() {
    }

    public Department(long departmentId){
        this.departmentId = departmentId;
    }

    public Department(String name, Person head, List<Person> personList, Company company) {
        this.name = name;
        this.head = head;
        this.personList = personList;
        this.company = company;
    }

    public Department(String login, String departmentName, long departmentId){
        Person person = new Person(login);
        this.head = person;

        this.departmentId = departmentId;

        this.name = departmentName;
    }

    public Department(long personId, String departmentName, long departmentId){
        Person person = new Person(personId);
        this.head = person;

        this.departmentId = departmentId;

        this.name = departmentName;
    }

    public Department(String login, long companyId, String departmentName){
        Person person = new Person(login);
        this.head = person;

        Company company = new Company();
        company.setCompanyId(companyId);
        this.company = company;

        this.name = departmentName;
    }


    public Department(long personId, long companyId, String departmentName){
        Person person = new Person(personId);
        this.head = person;

        Company company = new Company();
        company.setCompanyId(companyId);
        this.company = company;

        this.name = departmentName;
    }


    public long getDepartmentId() {
        return departmentId;
    }

    public void setDepartmentId(long departmentId) {
        this.departmentId = departmentId;
    }


    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }


    public Person getHead() {
        return head;
    }

    public void setHead(Person head) {
        this.head = head;
    }


    public List<Person> getPersonList() {
        return personList;
    }

    public void setPersonList(List<Person> personList) {
        this.personList = personList;
    }


    public Company getCompany() {
        return company;
    }

    public void setCompany(Company company) {
        this.company = company;
    }

    public long getNumberOfstaff() {
        return numberOfstaff;
    }

    public void setNumberOfstaff(long numberOfstaff) {
        this.numberOfstaff = numberOfstaff;
    }

    public List<Gas> getGases() {
        return gases;
    }

    public void setGases(List<Gas> gases) {
        this.gases = gases;
    }
}
