package kz.gasstation.spring.dao.entities;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import org.hibernate.annotations.Fetch;
import org.hibernate.annotations.FetchMode;

import javax.persistence.*;
import java.util.Date;
import java.util.List;

@Entity
@Table(name = "shift")
@JsonIgnoreProperties(ignoreUnknown = true)
public class Shift {

    @Id
    @Column(name = "shiftId")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long shiftId;

    @Column(name = "status")
    private String status;

    @Column(name = "start")
    private Date start;

    @Column(name = "end")
    private Date end;

    @Column(name = "profit")
    private Long profit;

    @OneToMany(fetch = FetchType.EAGER,mappedBy = "shiftId",cascade = CascadeType.MERGE)
    @Fetch(value = FetchMode.JOIN)
    private List<TransactionHistory> transactions;

    @ManyToOne
    @JoinColumn(name = "personId",nullable = false)
    private Person personId;

    public Shift(){
    }

    public Shift(long shiftId){
        this.shiftId = shiftId;
    }

    public Shift(long shiftId, Date endDate, Date startDate) {
        this.shiftId = shiftId;
        this.end = endDate;
        this.start = startDate;
    }

    public Shift(long shiftId,Date start, Date end, Long profit) {
        this.shiftId = shiftId;
        this.start = start;
        this.end = end;
        this.profit = profit;
    }

    public long getShiftId() {
        return shiftId;
    }

    public void setShiftId(long shiftId) {
        this.shiftId = shiftId;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public Date getStart() {
        return start;
    }

    public void setStart(Date start) {
        this.start = start;
    }

    public Date getEnd() {
        return end;
    }

    public void setEnd(Date end) {
        this.end = end;
    }

    public List<TransactionHistory> getTransactions() {
        return transactions;
    }

    public void setTransactions(List<TransactionHistory> transactions) {
        this.transactions = transactions;
    }

    public Person getPersonId() {
        return personId;
    }

    public void setPersonId(Person personId) {
        this.personId = personId;
    }

    public Long getProfit() {
        return profit;
    }

    public void setProfit(Long profit) {
        this.profit = profit;
    }
}
