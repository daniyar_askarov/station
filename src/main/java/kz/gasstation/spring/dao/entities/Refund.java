package kz.gasstation.spring.dao.entities;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

@JsonIgnoreProperties(ignoreUnknown = true)
public class Refund {

    private String login;
    private long balance;
    private String accountName;

    public Refund() {
    }

    public Refund(@JsonProperty("login") String login,@JsonProperty("cash") long balance,
                  @JsonProperty("account") String accountName, @JsonProperty("transactionId") String transactionId) {
        this.login = login;
        this.balance = balance;
        this.accountName = accountName;
    }

    public String getLogin() {
        return login;
    }

    public void setLogin(String login) {
        this.login = login;
    }

    public long getBalance() {
        return balance;
    }

    public void setBalance(long balance) {
        this.balance = balance;
    }

    public String getAccountName() {
        return accountName;
    }

    public void setAccountName(String accountName) {
        this.accountName = accountName;
    }
}
