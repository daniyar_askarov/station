package kz.gasstation.spring.dao.entities;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonRootName;

import javax.persistence.*;

@Entity
@Table(name = "paymentwait")
@JsonRootName("paymentwait")
public class PaymentWait {
    @JsonIgnoreProperties(ignoreUnknown = true)

    @Id
    @Column(name = "paymentId")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long paymentId;

    @Column(name = "balance")
    private String balance;

    @Column(name = "login")
    private String login;

    @Column(name = "personId")
    private long personId;

    @Column(name = "departmentId")
    private long departmentId;

    public PaymentWait() {
    }

    public PaymentWait(@JsonProperty("paymentId") long paymentId, @JsonProperty("balance") String balance,@JsonProperty("login") String login,
                           @JsonProperty("personId") long personId,@JsonProperty("departmentId") long departmentId) {
        this.paymentId = paymentId;
        this.balance = balance;
        this.login = login;
        this.personId = personId;
        this.departmentId = departmentId;
    }

    public long getPaymentId() {
        return paymentId;
    }

    public void setPaymentId(long paymentId) {
        this.paymentId = paymentId;
    }

    public String getBalance() {
        return balance;
    }

    public void setBalance(String balance) {
        this.balance = balance;
    }

    public String getLogin() {
        return login;
    }

    public void setLogin(String login) {
        this.login = login;
    }

    public long getPersonId() {
        return personId;
    }

    public void setPersonId(long personId) {
        this.personId = personId;
    }

    public long getDepartmentId() {
        return departmentId;
    }

    public void setDepartmentId(long departmentId) {
        this.departmentId = departmentId;
    }
}
