package kz.gasstation.spring.dao.entities;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import java.util.List;
import java.util.Map;

@JsonIgnoreProperties(ignoreUnknown = true)
public class StationGases {

    private Department department;
    private Map<String, Double> gases;

    public StationGases() {
    }

    public StationGases(Department department, Map<String, Double> gases) {
        this.department = department;
        this.gases = gases;
    }

    public Department getDepartment() {
        return department;
    }

    public void setDepartment(Department department) {
        this.department = department;
    }

    public Map<String, Double> getGases() {
        return gases;
    }

    public void setGases(Map<String, Double> gases) {
        this.gases = gases;
    }
}
