package kz.gasstation.spring.dao.entities;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonRootName;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import kz.gasstation.spring.dao.deserialize.DashboardDeserialize;
import kz.gasstation.spring.dao.serialize.PersonSerialize;
import org.hibernate.annotations.Fetch;
import org.hibernate.annotations.FetchMode;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Entity
@Table(name = "person")
@JsonRootName("PersonInfo")
@JsonIgnoreProperties(ignoreUnknown = true)
public class Person {

    @Id
    @Column(name = "personId")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long personId;

    @Column(name = "login",unique = true)
    private String login;

    @Column(name = "password")
    private String password;

    @Column(name = "name")
    private String name;

    @Column(name = "surname")
    private String surname;

    @Column(name = "email")
    private String email;

    @Column(name = "phone")
    private String phone;

    @Column(name = "birthdate")
    private Date birthdate;

    @OneToOne(fetch = FetchType.EAGER,cascade = CascadeType.ALL)
    @JoinColumn(name = "activeAccount",nullable = true)
    private Account activeAccount;

    @OneToOne(mappedBy = "personId")
    private Role role;

    @ManyToOne
    @JoinColumn(name = "departmentId",nullable = true)
    private Department departmentId;

    @OneToMany(fetch = FetchType.EAGER,mappedBy = "personId",cascade = CascadeType.MERGE)
    @Fetch(value = FetchMode.SUBSELECT)
    private List<TransactionHistory> transactionHistories;

    @OneToMany(fetch = FetchType.EAGER,mappedBy = "personId",cascade = CascadeType.ALL)
    @Fetch(value = FetchMode.SUBSELECT)
    private List<Account> accounts;

    @OneToMany(fetch = FetchType.EAGER,mappedBy = "personId",cascade = CascadeType.ALL)
    @Fetch(value = FetchMode.SUBSELECT)
    private List<Shift> shifts;

    @OneToOne(mappedBy = "head")
    private Company company;

    @OneToOne(mappedBy = "head")
    private Department dep;

    public Person() {
    }

    public Person(long personId){
        this.personId = personId;
    }

    public Person(String login) {
        this.login = login;
    }

    public Person(String login, long departmentId) {
        this.login = login;

        Department department = new Department(departmentId);
        this.departmentId = department;
    }

    public Person (String login, String password, long cashierId){
        this.personId = cashierId;
        this.login = login;
        this.password = password;
    }

    public Person(long departmentId,String login) {
        this.login = login;

        Department department = new Department(departmentId);
        this.departmentId = department;
    }

    public Person(long departmentId,String login, String password) {
        this.login = login;
        this.password = password;

        Department department = new Department(departmentId);
        this.departmentId = department;
    }

    public Person(long personId, Date startDate, Date endDate) {
        this.personId = personId;

        List<TransactionHistory> list = new ArrayList<>();
        list.add(new TransactionHistory(startDate));
        list.add(new TransactionHistory(endDate));
        this.setTransactionHistories(list);
    }

    public Person(long personId, long departmentId) {
        this.personId = personId;

        Department department = new Department(departmentId);
        this.departmentId = department;
    }

    public Person(long personId, long departmentId, Date startDate, Date endDate) {
        this.personId = personId;

        Department department = new Department(departmentId);
        this.departmentId = department;

        List<TransactionHistory> list = new ArrayList<>();
        list.add(new TransactionHistory(startDate));
        list.add(new TransactionHistory(endDate));
        this.setTransactionHistories(list);
    }


    public Person(String login, String password) {
        this.login = login;
        this.password = password;
    }

    public Person(long personId, long headId, long price){
        this.personId = personId;

        Department department = new Department(headId);
        this.departmentId = department;

        Company company = new Company(price);
        this.company = company;
    }

    public Person(long personId,String password, String name, String surname, String email, String phone) {
        this.personId = personId;
        this.password = password;
        this.name = name;
        this.surname = surname;
        this.email = email;
        this.phone = phone;
    }

    public Person(long personId, String password, String email, String phone) {
        this.personId = personId;
        this.password = password;
        this.email = email;
        this.phone = phone;
    }

    public Person(String login, String password, String name, String surname, String email, String phone, Date birthdate) {
        this.login = login;
        this.password = password;
        this.name = name;
        this.surname = surname;
        this.email = email;
        this.phone = phone;
        this.birthdate = birthdate;
    }

    public Person(long id, String login, String password, String name, String surname, String email, String phone, Date birthdate) {
        this.personId = id;
        this.login = login;
        this.password = password;
        this.name = name;
        this.surname = surname;
        this.email = email;
        this.phone = phone;
        this.birthdate = birthdate;
    }

    public Person(String login, String password, String name, String surname, String email, String phone, Date birthdate, Account activeAccount, Role role) {
        this.login = login;
        this.password = password;
        this.name = name;
        this.surname = surname;
        this.email = email;
        this.phone = phone;
        this.birthdate = birthdate;
        this.activeAccount = activeAccount;
        this.role = role;
    }

    public Person(String login, String password, String name, String surname, String email, String phone, Date birthdate, Role role, Department departmentId, List<TransactionHistory> transactionHistories, List<Account> accounts, Company company, Department dep) {
        this.login = login;
        this.password = password;
        this.name = name;
        this.surname = surname;
        this.email = email;
        this.phone = phone;
        this.birthdate = birthdate;
        this.role = role;
        this.departmentId = departmentId;
        this.transactionHistories = transactionHistories;
        this.accounts = accounts;
        this.company = company;
        this.dep = dep;
    }

    public long getPersonId() {return personId;}

    public void setPersonId(long personId) {this.personId = personId;}


    public String getLogin() {
        return login;
    }

    public void setLogin(String login) {
        this.login = login;
    }


    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }


    public String getName() {return name;}

    public void setName(String name) {
        this.name = name;
    }


    public String getSurname() {
        return surname;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }


    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }


    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }


    public Date getBirthdate() {
        return birthdate;
    }

    public void setBirthdate(Date birthdate) {
        this.birthdate = birthdate;
    }


    public Department getDepartment() {
        return departmentId;
    }

    public void setDepartment(Department departmentId) {
        this.departmentId = departmentId;
    }

    public Role getRole() {
        return role;
    }

    public void setRole(Role role) {
        this.role = role;
    }

    public Department getDepartmentId() {
        return departmentId;
    }

    public void setDepartmentId(Department departmentId) {
        this.departmentId = departmentId;
    }

    public Company getCompany() {
        return company;
    }

    public void setCompany(Company company) {
        this.company = company;
    }

    public Department getDep() {
        return dep;
    }

    public void setDep(Department dep) {
        this.dep = dep;
    }

    public List<TransactionHistory> getTransactionHistories() {
        return transactionHistories;
    }

    public void setTransactionHistories(List<TransactionHistory> transactionHistories) {
        this.transactionHistories = transactionHistories;
    }

    public List<Account> getAccounts() {
        return accounts;
    }

    public void setAccounts(List<Account> accounts) {
        this.accounts = accounts;
    }

    public Account getActiveAccount() {
        return activeAccount;
    }

    public void setActiveAccount(Account activeAccount) {
        this.activeAccount = activeAccount;
    }

    public List<Shift> getShifts() {
        return shifts;
    }

    public void setShifts(List<Shift> shifts) {
        this.shifts = shifts;
    }

    @Override
    public String toString() {
        return "Person {" +
                "PersonId=" + this.personId +
                ", Password=" + this.password +
                ", Email=" + this.email +
                ", Phone=" + this.phone + '\'' +
                "}";
    }
}
