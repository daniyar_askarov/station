package kz.gasstation.spring.dao.entities;

import javax.persistence.*;

@Entity
@Table(name = "gas")
public class Gas {

    @Id
    @Column(name = "gasId")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long gasId;

    @ManyToOne
    @JoinColumn(name = "departmentId",nullable = false)
    private Department department;

    @Column(name = "name")
    private String name;

    @Column(name = "price")
    private long price;

    public Gas() {
    }

    public Gas(long gasId,long price) {
        this.gasId = gasId;
        this.price = price;
    }

    public Gas(long departmentId, String name, long price){
        Department department = new Department(departmentId);

        this.department = department;
        this.name = name;
        this.price = price;
    }

    public Gas(Department department, String name, long price) {
        this.department = department;
        this.name = name;
        this.price = price;
    }

    public Gas(long gasId) {
        this.gasId = gasId;
    }

    public Gas(String name) {
        this.name = name;
    }

    public long getGasId() {
        return gasId;
    }

    public void setGasId(long gasId) {
        this.gasId = gasId;
    }

    public Department getDepartment() {
        return department;
    }

    public void setDepartment(Department department) {
        this.department = department;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public long getPrice() {
        return price;
    }

    public void setPrice(long price) {
        this.price = price;
    }
}
