package kz.gasstation.spring.dao.deserialize;

import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.deser.std.StdDeserializer;
import kz.gasstation.spring.dao.entities.Department;

import java.io.IOException;

public class MDepartmentCreationDeserialize extends StdDeserializer {

    public MDepartmentCreationDeserialize(){
        this((Class) null);
    }

    protected MDepartmentCreationDeserialize(Class vc) {
        super(vc);
    }

    protected MDepartmentCreationDeserialize(StdDeserializer src) {
        super(src);
    }

    @Override
    public Object deserialize(JsonParser jsonParser, DeserializationContext deserializationContext) throws IOException, JsonProcessingException {
        JsonNode node =jsonParser.getCodec().readTree(jsonParser);
        String login = node.get("login").asText();
        long companyId = node.get("companyId").asLong();
        String departmentName = node.get("departmentName").asText();
        return new Department(login,companyId,departmentName);
    }
}
