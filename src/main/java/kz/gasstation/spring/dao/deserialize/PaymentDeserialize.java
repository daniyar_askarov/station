package kz.gasstation.spring.dao.deserialize;

import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.deser.std.StdDeserializer;
import kz.gasstation.spring.dao.entities.TransactionHistory;

import java.io.IOException;

public class PaymentDeserialize extends StdDeserializer {

    public PaymentDeserialize(){
        this((Class) null);
    }

    public PaymentDeserialize(Class vc) {
        super(vc);
    }

    public PaymentDeserialize(StdDeserializer src) {
        super(src);
    }

    public Object deserialize(JsonParser jsonParser, DeserializationContext deserializationContext) throws IOException, JsonProcessingException {
        JsonNode node = jsonParser.getCodec().readTree(jsonParser);
        long personId = node.get("clientId").asLong();
        long shiftId = node.get("shiftId").asLong();
        long price = node.get("price").asLong();
        long priceLiter = node.get("priceLiter").asLong();
        String activeAccountName = node.get("accountName").asText();
        String gas = node.get("gas").asText();
        return new TransactionHistory(personId,shiftId,price,priceLiter,activeAccountName,gas);
    }
}
