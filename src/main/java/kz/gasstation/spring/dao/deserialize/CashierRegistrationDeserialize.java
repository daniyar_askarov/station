package kz.gasstation.spring.dao.deserialize;

import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.deser.std.StdDeserializer;
import kz.gasstation.spring.dao.entities.Person;

import java.io.IOException;

public class CashierRegistrationDeserialize extends StdDeserializer {

    public CashierRegistrationDeserialize(){
        this(null);
    }

    protected CashierRegistrationDeserialize(Class vc) {
        super(vc);
    }

    @Override
    public Object deserialize(JsonParser jsonParser, DeserializationContext deserializationContext) throws IOException, JsonProcessingException {
        JsonNode node = jsonParser.getCodec().readTree(jsonParser);
        long departmentId = node.get("departmentId").asLong();
        String login = node.get("login").asText();
        String password = node.get("password").asText();
        return new Person(departmentId,login,password);
    }
}
