package kz.gasstation.spring.dao.deserialize;

import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.deser.std.StdDeserializer;
import kz.gasstation.spring.dao.entities.Gas;

import java.io.IOException;

public class AddGasDeserialize extends StdDeserializer {

    public AddGasDeserialize() {
        this(null);
    }

    protected AddGasDeserialize(Class vc) {
        super(vc);
    }

    @Override
    public Object deserialize(JsonParser jsonParser, DeserializationContext deserializationContext) throws IOException, JsonProcessingException {
        JsonNode node = jsonParser.getCodec().readTree(jsonParser);
        long departmentId = node.get("departmentId").asLong();
        String name = node.get("name").asText();
        long price = node.get("price").asLong();
        return new Gas(departmentId, name, price);
    }
}