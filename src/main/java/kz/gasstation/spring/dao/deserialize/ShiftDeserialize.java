package kz.gasstation.spring.dao.deserialize;

import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.deser.std.StdDeserializer;
import kz.gasstation.spring.dao.entities.Shift;

import java.io.IOException;

public class ShiftDeserialize extends StdDeserializer{

    public ShiftDeserialize(){
        this((Class) null);
    }

    protected ShiftDeserialize(Class vc) {
        super(vc);
    }

    protected ShiftDeserialize(StdDeserializer src) {
        super(src);
    }

    @Override
    public Object deserialize(JsonParser jsonParser, DeserializationContext deserializationContext) throws IOException, JsonProcessingException {
        JsonNode node =jsonParser.getCodec().readTree(jsonParser);
        long shiftId = node.get("shiftId").asLong();
        return new Shift(shiftId);
    }
}
