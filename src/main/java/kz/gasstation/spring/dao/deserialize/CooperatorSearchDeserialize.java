package kz.gasstation.spring.dao.deserialize;

import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.deser.std.StdDeserializer;
import kz.gasstation.spring.dao.entities.Person;

import java.io.IOException;

public class CooperatorSearchDeserialize extends StdDeserializer {

    public CooperatorSearchDeserialize(){
        this((Class) null);
    }

    protected CooperatorSearchDeserialize(Class vc) {
        super(vc);
    }

    protected CooperatorSearchDeserialize(StdDeserializer src) {
        super(src);
    }

    @Override
    public Object deserialize(JsonParser jsonParser, DeserializationContext deserializationContext) throws IOException, JsonProcessingException {
        JsonNode node = jsonParser.getCodec().readTree(jsonParser);
        long departmentId = node.get("companyId").asLong();
        String loginPattern = node.get("pattern").asText();
        return new Person(departmentId,loginPattern);
    }
}
