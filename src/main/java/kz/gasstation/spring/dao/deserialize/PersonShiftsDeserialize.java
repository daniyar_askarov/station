package kz.gasstation.spring.dao.deserialize;

import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.deser.std.StdDeserializer;
import kz.gasstation.spring.dao.entities.Person;
import kz.gasstation.spring.dao.entities.Shift;

import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

public class PersonShiftsDeserialize extends StdDeserializer {

    public PersonShiftsDeserialize(){
        this((Class) null);
    }

    protected PersonShiftsDeserialize(Class vc) {
        super(vc);
    }

    protected PersonShiftsDeserialize(StdDeserializer src) {
        super(src);
    }

    @Override
    public Object deserialize(JsonParser jsonParser, DeserializationContext deserializationContext) throws IOException, JsonProcessingException {
        JsonNode node = jsonParser.getCodec().readTree(jsonParser);
        long departmentId = node.get("personId").asLong();
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
        Date startDate = new Date();
        Date endDate = new Date();
        try {
            startDate = dateFormat.parse(node.get("startDate").asText());
            endDate = dateFormat.parse(node.get("endDate").asText());
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return new Shift(departmentId, endDate, startDate);
    }
}
