package kz.gasstation.spring.dao.deserialize;

import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.deser.std.StdDeserializer;
import kz.gasstation.spring.dao.entities.Person;

import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

public class RegistrationDeserialize extends StdDeserializer {

    public RegistrationDeserialize(){
        this((Class) null);
    }

    protected RegistrationDeserialize(Class vc) {
        super(vc);
    }

    protected RegistrationDeserialize(StdDeserializer src) {
        super(src);
    }

    @Override
    public Object deserialize(JsonParser jsonParser, DeserializationContext deserializationContext) throws IOException, JsonProcessingException {
        JsonNode node = jsonParser.getCodec().readTree(jsonParser);
        String login = node.get("login").asText();
        String password = node.get("password").asText();
        String name = node.get("name").asText();
        String surname = node.get("surname").asText();
        String phone = node.get("phone").asText();
        String email = node.get("email").asText();
        String birthdate = node.get("birthdate").asText();
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-dd-MM");
        Date date = new Date();
        try {
            date = dateFormat.parse(birthdate);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return new Person(login, password, name, surname, email, phone, date);
    }
}
