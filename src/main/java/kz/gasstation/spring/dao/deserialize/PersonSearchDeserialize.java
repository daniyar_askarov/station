package kz.gasstation.spring.dao.deserialize;

import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.deser.std.StdDeserializer;
import kz.gasstation.spring.dao.entities.Person;

import java.io.IOException;

public class PersonSearchDeserialize extends StdDeserializer {

    public PersonSearchDeserialize(){
        this((Class) null);
    }

    protected PersonSearchDeserialize(Class vc) {
        super(vc);
    }

    protected PersonSearchDeserialize(StdDeserializer src) {
        super(src);
    }

    @Override
    public Object deserialize(JsonParser jsonParser, DeserializationContext deserializationContext) throws IOException, JsonProcessingException {
        JsonNode node = jsonParser.getCodec().readTree(jsonParser);
        String loginPattern = node.get("pattern").asText();
        return new Person(loginPattern);
    }
}
