package kz.gasstation.spring.dao.deserialize;

import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.deser.std.StdDeserializer;
import kz.gasstation.spring.dao.entities.Department;

import java.io.IOException;

public class DepartmentUpdateDeserialize extends StdDeserializer {

    public DepartmentUpdateDeserialize() {
        this((Class) null);
    }

    protected DepartmentUpdateDeserialize(Class vc) {
        super(vc);
    }

    protected DepartmentUpdateDeserialize(StdDeserializer src) {
        super(src);
    }

    @Override
    public Object deserialize(JsonParser jsonParser, DeserializationContext deserializationContext) throws IOException, JsonProcessingException {
        JsonNode node = jsonParser.getCodec().readTree(jsonParser);
        long headId = node.get("headId").asLong();
        long departmentId = node.get("departmentId").asLong();
        String departmentName = node.get("departmentName").asText();
        return new Department(headId, departmentName, departmentId);
    }
}
