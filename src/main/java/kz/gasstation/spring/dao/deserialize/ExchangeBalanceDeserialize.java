package kz.gasstation.spring.dao.deserialize;

import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.deser.std.StdDeserializer;
import kz.gasstation.spring.dao.entities.Person;

import java.io.IOException;

public class ExchangeBalanceDeserialize extends StdDeserializer<Person> {

    public ExchangeBalanceDeserialize(){
        this((Class) null);
    }

    protected ExchangeBalanceDeserialize(Class vc) {
        super(vc);
    }

    protected ExchangeBalanceDeserialize(StdDeserializer src) {
        super(src);
    }

    @Override
    public Person deserialize(JsonParser jsonParser, DeserializationContext deserializationContext) throws IOException, JsonProcessingException {
        JsonNode node =jsonParser.getCodec().readTree(jsonParser);
        long headId = node.get("headId").asLong();
        long cooperatorId = node.get("cooperatorId").asLong();
        long price = node.get("price").asLong();
        return new Person(cooperatorId, headId, price);
    }
}
