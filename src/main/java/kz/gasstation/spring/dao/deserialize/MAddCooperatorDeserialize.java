package kz.gasstation.spring.dao.deserialize;

import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.deser.std.StdDeserializer;
import kz.gasstation.spring.dao.entities.Person;

import java.io.IOException;

public class MAddCooperatorDeserialize extends StdDeserializer {

    public MAddCooperatorDeserialize(){
        this((Class) null);
    }

    protected MAddCooperatorDeserialize(Class vc) {
        super(vc);
    }

    protected MAddCooperatorDeserialize(StdDeserializer src) {
        super(src);
    }

    @Override
    public Object deserialize(JsonParser jsonParser, DeserializationContext deserializationContext) throws IOException, JsonProcessingException {
        JsonNode node =jsonParser.getCodec().readTree(jsonParser);
        String login = node.get("login").asText();
        long departmentId = node.get("departmentId").asLong();
        return new Person(login,departmentId);
    }
}
