package kz.gasstation.spring.dao.deserialize;

import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.deser.std.StdDeserializer;
import kz.gasstation.spring.dao.entities.Person;

import java.io.IOException;

public class PersonDeserialize extends StdDeserializer {

    public PersonDeserialize(){
        this((Class) null);
    }

    protected PersonDeserialize(Class vc) {
        super(vc);
    }

    protected PersonDeserialize(StdDeserializer src) {
        super(src);
    }

    public Object deserialize(JsonParser jsonParser, DeserializationContext deserializationContext) throws IOException, JsonProcessingException {
        JsonNode node = jsonParser.getCodec().readTree(jsonParser);
        long personId = node.get("personId").asLong();
        String name = node.get("name").asText();
        String surname = node.get("surname").asText();
        String password = node.get("password").asText();
        String email = node.get("email").asText();
        String phone = node.get("phone").asText();
        return new Person(personId,password,name,surname,email,phone);
    }
}