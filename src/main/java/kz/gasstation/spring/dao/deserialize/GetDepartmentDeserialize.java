package kz.gasstation.spring.dao.deserialize;

import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.deser.std.StdDeserializer;
import kz.gasstation.spring.dao.entities.Department;
import kz.gasstation.spring.dao.entities.Person;

import java.io.IOException;

public class GetDepartmentDeserialize extends StdDeserializer {

    public GetDepartmentDeserialize(){
        this((Class) null);
    }

    protected GetDepartmentDeserialize(Class vc) {
        super(vc);
    }

    protected GetDepartmentDeserialize(StdDeserializer src) {
        super(src);
    }

    @Override
    public Object deserialize(JsonParser jsonParser, DeserializationContext deserializationContext) throws IOException, JsonProcessingException {
        JsonNode node = jsonParser.getCodec().readTree(jsonParser);
        long departmentId = node.get("departmentId").asLong();
        return new Department(departmentId);
    }
}
