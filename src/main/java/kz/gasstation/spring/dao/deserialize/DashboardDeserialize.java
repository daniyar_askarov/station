package kz.gasstation.spring.dao.deserialize;

import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.deser.std.StdDeserializer;
import kz.gasstation.spring.dao.entities.Person;

import java.io.IOException;

public class DashboardDeserialize extends StdDeserializer {

    public DashboardDeserialize(){
        this((Class) null);
    }

    public DashboardDeserialize(Class vc) {
        super(vc);
    }

    public DashboardDeserialize(StdDeserializer src) {
        super(src);
    }

    public Object deserialize(JsonParser jsonParser, DeserializationContext deserializationContext) throws IOException, JsonProcessingException {
        JsonNode node =jsonParser.getCodec().readTree(jsonParser);
        String login = node.get("login").asText();
        String password = node.get("password").asText();
        return new Person(login,password);
    }
}
