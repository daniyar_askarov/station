package kz.gasstation.spring.dao.implementations;

import kz.gasstation.spring.dao.entities.*;
import kz.gasstation.spring.dao.interfaces.ICommonDao;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.Query;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class HibernateDao implements ICommonDao {

    private SessionFactory sessionFactory;
    private PasswordEncoder passwordEncoder;

    public SessionFactory getSessionFactory() {
        return sessionFactory;
    }

    public void setSessionFactory(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }

    @Transactional(propagation = Propagation.REQUIRED)
    public List<Person> getPersonsList() {
        Session session = sessionFactory.getCurrentSession();
        Query query = session.createQuery("from Person");
        List<Person> list = query.getResultList();
        return list;
    }

    @Transactional(propagation = Propagation.REQUIRED)
    public void addUserPayment(PaymentWait payment){
        Session session = sessionFactory.getCurrentSession();
        session.saveOrUpdate(payment);
    }


    @Transactional(propagation = Propagation.REQUIRED)
    public Role getRoleByPersonId(long personId){
        Session session = sessionFactory.getCurrentSession();
        Query query = session.createQuery("from Role where personId =" + personId);
        Role role = (Role) query.getSingleResult();
        return role;
    }

    @Transactional(propagation = Propagation.REQUIRED)
    public Person getPersonByLoginAndPassword(String login, String password) {
        passwordEncoder = new BCryptPasswordEncoder();
        List<Person> list = getPersonsList();
        for (Person person: list){
            if(person.getLogin().equals(login) && passwordEncoder.matches(password,person.getPassword())) return person;
        }
        return null;
    }

    @Transactional(propagation = Propagation.REQUIRED)
    public Person getPersonById(long id) {
        Session session = sessionFactory.getCurrentSession();
        Query query = session.createQuery("from Person where personId =" + id);
        Person person = (Person) query.getSingleResult();
        return person;
    }

    @Transactional(propagation = Propagation.REQUIRED)
    public Shift getShiftId(String departmentId) {
        Session session = sessionFactory.getCurrentSession();
        Query query = session.createQuery("FROM Shift s WHERE s.personId IN ( SELECT p.personId FROM Person p WHERE p.departmentId = " + departmentId + " ) AND s.status = 'close' ");
        Shift shift = (Shift) query.getSingleResult();
        return shift;
    }

    @Transactional(propagation = Propagation.REQUIRED)
    public Account getActiveAccount(long personId, String accountName){
        List<Account> list = getAccountListByPersonId(personId);
        for (Account account: list){
            if(account.getName().equals(accountName)) return account;
        }
        return null;
    }

    @Transactional(propagation = Propagation.REQUIRED)
    public void updateAccountBalance(Account account) {
        Session session = sessionFactory.getCurrentSession();
        session.saveOrUpdate(account);
    }

    @Transactional(propagation = Propagation.REQUIRED)
    public void updatePerson(Person person){
        passwordEncoder = new BCryptPasswordEncoder();
        person.setPassword(passwordEncoder.encode(person.getPassword()));
        Session session = sessionFactory.getCurrentSession();
        session.saveOrUpdate(person);
    }

    @Transactional(propagation = Propagation.REQUIRED)
    public void mergePerson(Person person){
        Session session = sessionFactory.getCurrentSession();
        session.update(person);
    }

    @Transactional(propagation = Propagation.REQUIRED)
    public void deleteAccount(Account account){
        Session session = sessionFactory.getCurrentSession();
        session.delete(account);
    }

    @Transactional(propagation = Propagation.REQUIRED)
    public void updateRole(Role role){
        Session session = sessionFactory.getCurrentSession();
        session.saveOrUpdate(role);
    }

    @Transactional(propagation = Propagation.REQUIRED)
    public List<Department> getDepartmentList(){
        Session session = sessionFactory.getCurrentSession();
//        session.beginTransaction();
        Query query = session.createQuery("from Department");
        List<Department> list = query.getResultList();
/*        session.getTransaction().commit();
        session.getTransaction().rollback();*/
        return list;
    }

    @Transactional(propagation = Propagation.REQUIRED)
    public Department getDepartmentById(long id){
        Session session = sessionFactory.getCurrentSession();
        Query query = session.createQuery("from Department where departmentId =" + id);
        Department department = (Department) query.getSingleResult();
        return department;
    }

    @Transactional(propagation = Propagation.REQUIRED)
    public Department getDepartmentByHeadId(long id){
        Session session = sessionFactory.getCurrentSession();
        Query query = session.createQuery("from Department where head =" + id);
        Department department = (Department) query.getSingleResult();
        return department;
    }

    @Transactional(propagation = Propagation.REQUIRED)
    public void deleteDepartment(Department department){
        Session session = sessionFactory.getCurrentSession();
        session.delete(department);
    }

    @Transactional(propagation = Propagation.REQUIRED)
    public void addTransaction(TransactionHistory transactionHistory){
        Session session = sessionFactory.getCurrentSession();
        session.saveOrUpdate(transactionHistory);
    }

    @Transactional(propagation = Propagation.REQUIRED)
    public void updateTransaction(TransactionHistory transactionHistory){
        Session session = sessionFactory.getCurrentSession();
        session.saveOrUpdate(transactionHistory);
    }

    @Transactional(propagation = Propagation.REQUIRED)
    public List<TransactionHistory> getTransactionsByPersonId(long id, long page, Date startDate, Date endDate) {
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd 23:59:59");
        SimpleDateFormat dateFormat1 = new SimpleDateFormat("yyyy-MM-dd 00:00:00");
        Session session = sessionFactory.getCurrentSession();
        Query query = session.createQuery("FROM TransactionHistory WHERE personId = " + id + " AND (date BETWEEN '" + dateFormat1.format(startDate) + "' AND '" + dateFormat.format(endDate) + "') ORDER BY date DESC").setFirstResult((int) (10*(page-1))).setMaxResults(10);
        List<TransactionHistory> list = query.getResultList();
        return list;
    }

    @Transactional(propagation = Propagation.REQUIRED)
    public TransactionHistory getTransactionsById(long id) {
        Session session = sessionFactory.getCurrentSession();
        Query query = session.createQuery("FROM TransactionHistory WHERE transactionId = " + id);
        TransactionHistory history = (TransactionHistory) query.getSingleResult();
        return history;
    }

    @Transactional(propagation = Propagation.REQUIRED)
    public List<TransactionHistory> getTransactionsByPersonId(long id, Date startDate, Date endDate) {
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd 23:59:59");
        SimpleDateFormat dateFormat1 = new SimpleDateFormat("yyyy-MM-dd 00:00:00");
        Session session = sessionFactory.getCurrentSession();
        Query query = session.createQuery("FROM TransactionHistory WHERE personId = " + id + " AND (date BETWEEN " + dateFormat1.format(startDate) + " AND " + dateFormat.format(endDate) + ") ORDER BY date DESC");
        List<TransactionHistory> list = query.getResultList();
        return list;
    }

    @Transactional(propagation = Propagation.REQUIRED)
    public List<Company> getCompanyList(){
        Session session = sessionFactory.getCurrentSession();
        Query query = session.createQuery("from Company");
        List<Company> list = query.getResultList();
        return list;
    }

    @Transactional(propagation = Propagation.REQUIRED)
    public Company getCompanyById(long id) {
        List<Company> list = getCompanyList();
        for (Company company: list){
            if(company.getCompanyId() == id) return company;
        }
        return null;
    }

    @Transactional(propagation = Propagation.REQUIRED)
    public Company getCompanyByHeadId(long headId) {
        List<Company> list = getCompanyList();
        for (Company company: list){
            if(company.getHead().getPersonId() == headId) return company;
        }
        return null;
    }

    @Transactional(propagation = Propagation.REQUIRED)
    public List<Account> getAccountListByPersonId(long id) {
        Session session = sessionFactory.getCurrentSession();
        Query query = session.createQuery("FROM Account WHERE personId = " + id).setMaxResults(2);
        List<Account> list = query.getResultList();
        return list;
    }

    @Transactional(propagation = Propagation.REQUIRED)
    public List<Department> getDepartmentListByCompanyId(long id){
        Session session = sessionFactory.getCurrentSession();
        Query query = session.createQuery("FROM Department WHERE companyId = " + id + " ORDER BY name ASC");
        List<Department> list = query.getResultList();
        return list;
    }

    @Transactional(propagation = Propagation.REQUIRED)
    public Person getHeadByDepartmentId(long id){
        Session session = sessionFactory.getCurrentSession();
        Query query = session.createQuery("FROM Person p WHERE p.personId IN (SELECT d.head FROM Department d WHERE d.departmentId = " + id + ")");
        Person person = (Person) query.getSingleResult();
        return person;
    }

    @Transactional(propagation = Propagation.REQUIRED)
    public List<Person> getPersonListByDepartmentId(long personId, long departmentId){
        Session session = sessionFactory.getCurrentSession();
        Query query = session.createQuery("from Person where personId != " + personId + " AND departmentId =" + departmentId);
        List<Person> list = query.getResultList();
        return list;
    }



    @Transactional(propagation = Propagation.REQUIRED)
    public Company getCompanyByDepartmentId(long id){
        Session session = sessionFactory.getCurrentSession();
        Query query = session.createQuery("FROM Company c WHERE c.companyId IN (SELECT d.company FROM Department d WHERE d.departmentId = " + id + ")");
        Company company = (Company) query.getSingleResult();
        return company;
    }

    @Transactional(propagation = Propagation.REQUIRED)
    public long getCountOfPersons(long personId, long departmentId){
        Session session = sessionFactory.getCurrentSession();
        Query query = session.createQuery("select count(personId) from Person where personId != " + personId + " AND departmentId =" + departmentId);
        long count = (long) query.getSingleResult();
        return count;
    }

    @Transactional(propagation = Propagation.REQUIRED)
    public void createDepartment(Department department){
        Session session = sessionFactory.getCurrentSession();
        session.saveOrUpdate(department);
    }

    @Transactional(propagation = Propagation.REQUIRED)
    public void updateDepartment(Department department){
        Session session = sessionFactory.getCurrentSession();
        session.merge(department);
    }

    @Transactional(propagation = Propagation.REQUIRED)
    public List<Person> searchPersonsByPattern(String pattern) {
        Session session = sessionFactory.getCurrentSession();
        Query query = session.createQuery("FROM Person p WHERE p.personId NOT IN (SELECT r.personId FROM Role r WHERE r.role LIKE '%ROLE_WORKER%' OR r.role LIKE '%ROLE_HEAD%' OR r.role LIKE '%ROLE_DIRECTOR%') AND p.login LIKE '%" + pattern + "%'");
        // "FROM Person WHERE departmentId IS NULL AND login LIKE '%" + pattern + "%'"
        List<Person> list = query.getResultList();
        return list;
    }

    @Transactional(propagation = Propagation.REQUIRED)
    public List<Company> searchCompanyByDep(long departmentId) {
        Session session = sessionFactory.getCurrentSession();
        Query query = session.createQuery("FROM Company WHERE companyId = ANY (SELECT company FROM Department WHERE departmentId = ANY (SELECT departmentId FROM Person WHERE personId = ANY (SELECT personId FROM TransactionHistory WHERE shiftId = ANY (SELECT shiftId FROM Shift WHERE personId = ANY (SELECT personId FROM Person WHERE departmentId = "+ departmentId +")))))");
        List<Company> list = query.getResultList();
        return list;
    }

    @Transactional(propagation = Propagation.REQUIRED)
    public List<Person> searchCooperateByMail(long departmentId,String pattern) {
        Session session = sessionFactory.getCurrentSession();
        Query query = session.createQuery("FROM Person p WHERE p.personId != (SELECT d.head FROM Department d WHERE d.departmentId = " + departmentId + " )  AND p.departmentId =" + departmentId + " AND p.email LIKE '%" + pattern + "%'");
        List<Person> list = query.getResultList();
        return list;
    }

    @Transactional(propagation = Propagation.REQUIRED)
    public List<Person> searchCooperateByPhone(long departmentId,String pattern) {
        Session session = sessionFactory.getCurrentSession();
        Query query = session.createQuery("FROM Person p WHERE p.personId != (SELECT d.head FROM Department d WHERE d.departmentId = " + departmentId + " )  AND p.departmentId =" + departmentId + " AND p.phone LIKE '%" + pattern + "%'");
        List<Person> list = query.getResultList();
        return list;
    }

    @Transactional(propagation = Propagation.REQUIRED)
    public List<Person> searchCooperateByName(long departmentId,String pattern) {
        Session session = sessionFactory.getCurrentSession();
        Query query = session.createQuery("FROM Person p WHERE p.personId != (SELECT d.head FROM Department d WHERE d.departmentId = " + departmentId + " )  AND p.departmentId =" + departmentId + " AND p.name LIKE '%" + pattern + "%'");
        List<Person> list = query.getResultList();
        return list;
    }

    @Transactional(propagation = Propagation.REQUIRED)
    public Account getCoperateBalance(long personId){
        Session session = sessionFactory.getCurrentSession();
        Query query = session.createQuery("FROM Account WHERE name = 'Coperate' AND personId = " + personId);
        Account account = (Account) query.getSingleResult();
        return account;
    }

    @Transactional(propagation = Propagation.REQUIRED)
    public List<TransactionHistory> getCooperatorTransactions(long personId, Date startDate, Date endDate) {
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd 23:59:59");
        SimpleDateFormat dateFormat1 = new SimpleDateFormat("yyyy-MM-dd 00:00:00");
        Session session = sessionFactory.getCurrentSession();
        Query query = session.createQuery("FROM TransactionHistory WHERE balance = 'Coperate' AND personId = " + personId + " AND (date BETWEEN '" + dateFormat1.format(startDate) + "' AND '" + dateFormat.format(endDate) + "') ORDER BY date DESC");
        List<TransactionHistory> list = query.getResultList();
        return list;
    }

    @Transactional(propagation = Propagation.REQUIRED)
    public List<PaymentWait> getPaymentHistory(long departmentId) {
        Session session = sessionFactory.getCurrentSession();
        Query query = session.createQuery("FROM PaymentWait WHERE departmentId = " + departmentId + " ORDER BY paymentId DESC");
        List<PaymentWait> list = query.getResultList();
        return list;
    }

    @Transactional(propagation = Propagation.REQUIRED)
    public List<TransactionHistory> getPersonalTransactions(long personId, Date startDate, Date endDate) {
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd 23:59:59");
        SimpleDateFormat dateFormat1 = new SimpleDateFormat("yyyy-MM-dd 00:00:00");
        Session session = sessionFactory.getCurrentSession();
        Query query = session.createQuery("FROM TransactionHistory WHERE balance = 'Personal' AND personId = " + personId + " AND (date BETWEEN '" + dateFormat1.format(startDate) + "' AND '" + dateFormat.format(endDate) + "') ORDER BY date DESC");
        List<TransactionHistory> list = query.getResultList();
        return list;
    }

    @Transactional(propagation = Propagation.REQUIRED)
    public List<Person> getPersonListByNameAndSurname(long departmentId,String name, String surname) {
        Session session = sessionFactory.getCurrentSession();
        Query query = session.createQuery("FROM Person p WHERE p.personId != (SELECT d.head FROM Department d WHERE d.departmentId = " + departmentId + " )  AND p.departmentId =" + departmentId + " AND p.name LIKE '%" + name + "%' AND p.surname LIKE '%" + surname + "%'");
        List<Person> list = query.getResultList();
        return list;
    }

    @Transactional(propagation = Propagation.REQUIRED)
    public List<Person> searchPersonsByNameAndSurname(String name, String surname) {
        Session session = sessionFactory.getCurrentSession();
        Query query = session.createQuery("FROM Person p WHERE p.personId NOT IN (SELECT r.personId FROM Role r WHERE r.role LIKE '%ROLE_WORKER%' OR r.role LIKE '%ROLE_HEAD%' OR r.role LIKE '%ROLE_DIRECTOR%') AND p.name LIKE '%" + name + "%' AND p.surname LIKE '%" + surname + "%'");
        List<Person> list = query.getResultList();
        return list;
    }


    @Transactional(propagation = Propagation.REQUIRED)
    public List<Person> searchPersonsByMail(String pattern) {
        Session session = sessionFactory.getCurrentSession();
        Query query = session.createQuery("FROM Person p WHERE p.personId NOT IN (SELECT r.personId FROM Role r WHERE r.role LIKE '%ROLE_WORKER%' OR r.role LIKE '%ROLE_HEAD%' OR r.role LIKE '%ROLE_DIRECTOR%') AND p.email LIKE '%" + pattern + "%'");
        List<Person> list = query.getResultList();
        return list;
    }

    @Transactional(propagation = Propagation.REQUIRED)
    public List<Person> searchPersonsByPhone(String pattern) {
        Session session = sessionFactory.getCurrentSession();
        Query query = session.createQuery("FROM Person p WHERE p.personId NOT IN (SELECT r.personId FROM Role r WHERE r.role LIKE '%ROLE_WORKER%' OR r.role LIKE '%ROLE_HEAD%' OR r.role LIKE '%ROLE_DIRECTOR%')  AND p.phone LIKE '%" + pattern + "%'");
        List<Person> list = query.getResultList();
        return list;
    }

    @Transactional(propagation = Propagation.REQUIRED)
    public List<Person> searchHeadByNameAndSurname(long companyId, String name, String surname) {
        Session session = sessionFactory.getCurrentSession();
        Query query = session.createQuery("FROM Person p WHERE (p.personId NOT IN (SELECT r.personId FROM Role r WHERE r.role LIKE '%ROLE_WORKER%' OR r.role LIKE '%ROLE_HEAD%' OR r.role LIKE '%ROLE_DIRECTOR%' OR r.role LIKE '%ROLE_CASHIER%') OR (p.departmentId IN (SELECT d.departmentId FROM Department d WHERE d.company = " + companyId + ") AND p.personId IN (SELECT r.personId FROM Role r WHERE r.role LIKE '%ROLE_WORKER%' AND r.role NOT LIKE '%ROLE_CASHIER%'))) AND p.name LIKE '%" + name + "%' AND p.surname LIKE '%" + surname + "%'");
        List<Person> list = query.getResultList();
        return list;
    }

    @Transactional(propagation = Propagation.REQUIRED)
    public List<Person> searchHeadByMail(long companyId, String pattern) {
        Session session = sessionFactory.getCurrentSession();
        Query query = session.createQuery("FROM Person p WHERE (p.personId NOT IN (SELECT r.personId FROM Role r WHERE r.role LIKE '%ROLE_WORKER%' OR r.role LIKE '%ROLE_HEAD%' OR r.role LIKE '%ROLE_DIRECTOR%' OR r.role LIKE '%ROLE_CASHIER%') OR (p.departmentId IN (SELECT d.departmentId FROM Department d WHERE d.company = " + companyId + ") AND p.personId IN (SELECT r.personId FROM Role r WHERE r.role LIKE '%ROLE_WORKER%' AND r.role NOT LIKE '%ROLE_CASHIER%'))) AND p.email LIKE '%" + pattern + "%'");
        List<Person> list = query.getResultList();
        return list;
    }

    @Transactional(propagation = Propagation.REQUIRED)
    public List<Person> searchHeadByPhone(long companyId, String pattern) {
        Session session = sessionFactory.getCurrentSession();
        Query query = session.createQuery("FROM Person p WHERE (p.personId NOT IN (SELECT r.personId FROM Role r WHERE r.role LIKE '%ROLE_WORKER%' OR r.role LIKE '%ROLE_HEAD%' OR r.role LIKE '%ROLE_DIRECTOR%' OR r.role LIKE '%ROLE_CASHIER%') OR (p.departmentId IN (SELECT d.departmentId FROM Department d WHERE d.company = " + companyId + ") AND p.personId IN (SELECT r.personId FROM Role r WHERE r.role LIKE '%ROLE_WORKER%' AND r.role NOT LIKE '%ROLE_CASHIER%'))) AND p.phone LIKE '%" + pattern + "%'");
        List<Person> list = query.getResultList();
        return list;
    }

    @Transactional(propagation = Propagation.REQUIRED)
    public List<Person> searchHeadByPattern(long companyId, String pattern) {
        Session session = sessionFactory.getCurrentSession();
        Query query = session.createQuery("FROM Person p WHERE (p.personId NOT IN (SELECT r.personId FROM Role r WHERE r.role LIKE '%ROLE_WORKER%' OR r.role LIKE '%ROLE_HEAD%' OR r.role LIKE '%ROLE_DIRECTOR%' OR r.role LIKE '%ROLE_CASHIER%') OR (p.departmentId IN (SELECT d.departmentId FROM Department d WHERE d.company = " + companyId + ") AND p.personId IN (SELECT r.personId FROM Role r WHERE r.role LIKE '%ROLE_WORKER%' AND r.role NOT LIKE '%ROLE_CASHIER%'))) AND p.login LIKE '%" + pattern + "%'");
        // "FROM Person WHERE departmentId IS NULL AND login LIKE '%" + pattern + "%'"
        List<Person> list = query.getResultList();
        return list;
    }

    @Transactional(propagation = Propagation.REQUIRED)
    public Person getDirectorByDepartmentId(long departmentId) {
        Session session = sessionFactory.getCurrentSession();
        Query query = session.createQuery("FROM Person p WHERE p.personId IN (SELECT c.head FROM Company c WHERE c.companyId IN (SELECT d.company FROM Department d WHERE d.departmentId = " + departmentId + "))");
        Person person = (Person) query.getSingleResult();
        return person;
    }

    @Transactional(propagation = Propagation.REQUIRED)
    public Person getPersonByLogin(String login) {
        List<Person> list = getPersonsList();
        for (Person person: list){
            if(person.getLogin().equals(login)) return person;
        }
        return null;
    }

    @Transactional(propagation = Propagation.REQUIRED)
    public long getCountOfTransactions(long personId, Date startDate, Date endDate){
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd 23:59:59");
        SimpleDateFormat dateFormat1 = new SimpleDateFormat("yyyy-MM-dd 00:00:00");
        Session session = sessionFactory.getCurrentSession();
        Query query = session.createQuery("select count(transactionId) from TransactionHistory where personId = " + personId + " AND (date BETWEEN '" + dateFormat1.format(startDate) + "' AND '" + dateFormat.format(endDate) + "')");
        long count = (long) query.getSingleResult();
        return count;
    }

    @Transactional(propagation = Propagation.REQUIRED)
    public long getCountOfTransactions(long personId,String balance,Date startDate, Date endDate){
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd 23:59:59");
        SimpleDateFormat dateFormat1 = new SimpleDateFormat("yyyy-MM-dd 00:00:00");
        Session session = sessionFactory.getCurrentSession();
        Query query = session.createQuery("select count(transactionId) from TransactionHistory where balance = '" + balance + "' AND personId = " + personId + " AND (date BETWEEN '" + dateFormat1.format(startDate) + "' AND '" + dateFormat.format(endDate) + "')");
        long count = (long) query.getSingleResult();
        return count;
    }

    @Transactional(propagation = Propagation.REQUIRED)
    public List<Person> searchIntoCompanyByNameAndSurname(long companyId, String name, String surname) {
        Session session = sessionFactory.getCurrentSession();
        Query query = session.createQuery("FROM Person p WHERE p.departmentId IN (SELECT d.departmentId FROM Department d WHERE d.company = " + companyId + ") AND p.personId IN (SELECT r.personId FROM Role r WHERE r.role LIKE '%ROLE_WORKER%' OR r.role LIKE '%ROLE_HEAD%') AND p.name LIKE '%" + name + "%' AND p.surname LIKE '%" + surname + "%'");
        List<Person> list = query.getResultList();
        return list;
    }

    @Transactional(propagation = Propagation.REQUIRED)
    public List<Person> searchIntoCompanyByMail(long companyId, String pattern) {
        Session session = sessionFactory.getCurrentSession();
        Query query = session.createQuery("FROM Person p WHERE p.departmentId IN (SELECT d.departmentId FROM Department d WHERE d.company = " + companyId + ") AND p.personId IN (SELECT r.personId FROM Role r WHERE r.role LIKE '%ROLE_WORKER%' OR r.role LIKE '%ROLE_HEAD%') AND p.personId IN (SELECT r.personId FROM Role r WHERE r.role LIKE '%ROLE_WORKER%'))) AND p.email LIKE '%" + pattern + "%'");
        List<Person> list = query.getResultList();
        return list;
    }

    @Transactional(propagation = Propagation.REQUIRED)
    public List<Person> searchIntoCompanyByPhone(long companyId, String pattern) {
        Session session = sessionFactory.getCurrentSession();
        Query query = session.createQuery("FROM Person p WHERE p.departmentId IN (SELECT d.departmentId FROM Department d WHERE d.company = " + companyId + ") AND p.personId IN (SELECT r.personId FROM Role r WHERE r.role LIKE '%ROLE_WORKER%' OR r.role LIKE '%ROLE_HEAD%') AND p.phone LIKE '%" + pattern + "%'");
        List<Person> list = query.getResultList();
        return list;
    }

    @Transactional(propagation = Propagation.REQUIRED)
    public List<Person> searchIntoCompanyByPattern(long companyId, String pattern) {
        Session session = sessionFactory.getCurrentSession();
        Query query = session.createQuery("FROM Person p WHERE p.departmentId IN (SELECT d.departmentId FROM Department d WHERE d.company = " + companyId + ") AND p.personId IN (SELECT r.personId FROM Role r WHERE r.role LIKE '%ROLE_WORKER%' OR r.role LIKE '%ROLE_HEAD%') AND p.login LIKE '%" + pattern + "%'");
        List<Person> list = query.getResultList();
        return list;
    }

    @Transactional(propagation = Propagation.REQUIRED)
    public void registration(Person person) {
        Session session = sessionFactory.getCurrentSession();
        session.save(person);
    }

    @Transactional(propagation = Propagation.REQUIRED)
    public void createRole(Role role) {
        Session session = sessionFactory.getCurrentSession();
        session.save(role);
    }

    @Transactional(propagation = Propagation.REQUIRED)
    public Shift getShiftById(long shiftId) {
        Session session = sessionFactory.getCurrentSession();
        Query query = session.createQuery("FROM Shift WHERE shiftId = " + shiftId);
        Shift shift = (Shift) query.getSingleResult();
        return shift;
    }

    @Transactional(propagation = Propagation.REQUIRED)
    public void createShift(Shift shift) {
        Session session = sessionFactory.getCurrentSession();
        session.save(shift);
    }

    @Transactional(propagation = Propagation.REQUIRED)
    public void updateShift(Shift shift) {
        Session session = sessionFactory.getCurrentSession();
        session.saveOrUpdate(shift);
    }

    @Transactional(propagation = Propagation.REQUIRED)
    public List<Shift> getShiftsByDate(long departmentId, Date startDate, Date endDate, long page) {
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd 23:59:59");
        SimpleDateFormat dateFormat1 = new SimpleDateFormat("yyyy-MM-dd 00:00:00");
        Session session = sessionFactory.getCurrentSession();
        Query query = session.createQuery("FROM Shift s WHERE s.personId IN (SELECT p.personId FROM Person p WHERE p.departmentId = " + departmentId + ") AND (s.start BETWEEN '" + dateFormat1.format(startDate) + "' AND '" + dateFormat.format(endDate) + "') ORDER BY s.start DESC").setFirstResult((int) (10*(page-1))).setMaxResults(10);
        List<Shift> list = query.getResultList();
        return list;
    }

    @Transactional(propagation = Propagation.REQUIRED)
    public long getCountOfShifts(long departmentId, Date startDate, Date endDate){
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd 23:59:59");
        SimpleDateFormat dateFormat1 = new SimpleDateFormat("yyyy-MM-dd 00:00:00");
        Session session = sessionFactory.getCurrentSession();
        Query query = session.createQuery("SELECT COUNT(s.shiftId) FROM Shift s WHERE s.personId IN (SELECT p.personId FROM Person p WHERE p.departmentId = " + departmentId + ") AND (s.start BETWEEN '" + dateFormat1.format(startDate) + "' AND '" + dateFormat.format(endDate) + "') ORDER BY s.start DESC");
        long count = (long) query.getSingleResult();
        return count;
    }

    @Transactional(propagation = Propagation.REQUIRED)
    public List<Shift> getOpenShiftsByDate(long departmentId, Date startDate, Date endDate) {
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd 23:59:59");
        SimpleDateFormat dateFormat1 = new SimpleDateFormat("yyyy-MM-dd 00:00:00");
        Session session = sessionFactory.getCurrentSession();
        Query query = session.createQuery("FROM Shift s WHERE s.status = 'open' AND s.personId IN (SELECT p.personId FROM Person p WHERE p.departmentId = " + departmentId + ") AND (s.start BETWEEN '" + dateFormat1.format(startDate) + "' AND '" + dateFormat.format(endDate) + "') ORDER BY s.start DESC");
        List<Shift> list = query.getResultList();
        return list;
    }

    @Transactional(propagation = Propagation.REQUIRED)
    public List<Shift> getCloseShiftsByDate(long departmentId, Date startDate, Date endDate) {
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd 23:59:59");
        SimpleDateFormat dateFormat1 = new SimpleDateFormat("yyyy-MM-dd 00:00:00");
        Session session = sessionFactory.getCurrentSession();
        Query query = session.createQuery("FROM Shift s WHERE s.status = 'close' AND s.personId IN (SELECT p.personId FROM Person p WHERE p.departmentId = " + departmentId + ") AND (s.start BETWEEN '" + dateFormat1.format(startDate) + "' AND '" + dateFormat.format(endDate) + "') ORDER BY s.start DESC");
        List<Shift> list = query.getResultList();
        return list;
    }

    @Transactional(propagation = Propagation.REQUIRED)
    public List<Shift> getPersonShiftsByDate(long personId, Date startDate, Date endDate) {
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd 23:59:59");
        SimpleDateFormat dateFormat1 = new SimpleDateFormat("yyyy-MM-dd 00:00:00");
        Session session = sessionFactory.getCurrentSession();
        Query query = session.createQuery("FROM Shift s WHERE s.personId = " + personId + " AND (s.start BETWEEN '" + dateFormat1.format(startDate) + "' AND '" + dateFormat.format(endDate) + "') ORDER BY s.start DESC");
        List<Shift> list = query.getResultList();
        return list;
    }

    @Transactional(propagation = Propagation.REQUIRED)
    public List<TransactionHistory> getTransactionsByShiftId(long shiftId) {
        Session session = sessionFactory.getCurrentSession();
        Query query = session.createQuery("FROM TransactionHistory WHERE shiftId = " + shiftId + " ORDER BY date DESC");
        List<TransactionHistory> list = query.getResultList();
        return list;
    }

    @Transactional(propagation = Propagation.REQUIRED)
    public void addGas(Gas gas) {
        Session session = sessionFactory.getCurrentSession();
        session.saveOrUpdate(gas);
    }

    @Transactional(propagation = Propagation.REQUIRED)
    public Gas getGasById(long gasId) {
        Session session = sessionFactory.getCurrentSession();
        Query query = session.createQuery("FROM Gas WHERE gasId = " + gasId);
        Gas gas = (Gas) query.getSingleResult();
        return gas;
    }

    @Transactional(propagation = Propagation.REQUIRED)
    public void deleteGas(Gas gas) {
        Session session = sessionFactory.getCurrentSession();
        session.delete(gas);
    }

    @Transactional(propagation = Propagation.REQUIRED)
    public void deletePayment(PaymentWait payment) {
        Session session = sessionFactory.getCurrentSession();
        session.delete(payment);
    }

    @Transactional(propagation = Propagation.REQUIRED)
    public List<Gas> getGasesByDepartmentId(long departmentId) {
        Session session = sessionFactory.getCurrentSession();
        Query query = session.createQuery("FROM Gas WHERE departmentId = " + departmentId);
        List<Gas> list = query.getResultList();
        return list;
    }

    @Transactional(propagation = Propagation.REQUIRED)
    public List<Person> getCashierByDepartmentId(long departmentId) {
        Session session = sessionFactory.getCurrentSession();
        Query query = session.createQuery("FROM Person p WHERE p.departmentId =" + departmentId + " AND p.personId IN (SELECT r.personId FROM Role r WHERE r.role LIKE '%ROLE_CASHIER%')");
        List<Person> list = query.getResultList();
        return list;
    }

    @Transactional(propagation = Propagation.REQUIRED)
    public Long getCountPrice(long shiftId) {
        Session session = sessionFactory.getCurrentSession();
        Query query = session.createQuery("select sum(price) from TransactionHistory where shiftId = " +  shiftId);
        Long sum = (Long) query.getSingleResult();
        return sum;
    }

    @Transactional(propagation = Propagation.REQUIRED)
    public long getSumPrice(long companyId, Date start, Date end) {
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd 23:59:59");
        SimpleDateFormat dateFormat1 = new SimpleDateFormat("yyyy-MM-dd 00:00:00");
        Session session = sessionFactory.getCurrentSession();
        Query query = session.createQuery("SELECT SUM(price) FROM TransactionHistory WHERE personId = ANY(SELECT personId FROM Person WHERE departmentId = ANY(SELECT departmentId FROM Department WHERE company = "+ companyId +"))AND balance = 'Coperate' AND date between '"+ dateFormat1.format(start) +"' AND '"+ dateFormat.format(end) +"'");
        long sum = 0;
        if(query.getSingleResult()!=null)
            sum = (long) query.getSingleResult();
        return sum;
    }

    @Transactional(propagation = Propagation.REQUIRED)
    public long getSumLiters(long companyId, Date start, Date end) {
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd 23:59:59");
        SimpleDateFormat dateFormat1 = new SimpleDateFormat("yyyy-MM-dd 00:00:00");
        Session session = sessionFactory.getCurrentSession();
        Query query = session.createQuery("SELECT SUM(priceLiter) FROM TransactionHistory WHERE personId = ANY(SELECT personId FROM Person WHERE departmentId = ANY(SELECT departmentId FROM Department WHERE company = "+ companyId +"))AND balance = 'Coperate' AND date between '"+ dateFormat1.format(start) +"' AND '"+ dateFormat.format(end) +"'");
        long sum = 0;
        if(query.getSingleResult()!=null)
            sum = (long) query.getSingleResult();
        return sum;
    }

    @Transactional(propagation = Propagation.REQUIRED)
    public long getSumPersonPrice(long personId, Date start, Date end) {
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd 23:59:59");
        SimpleDateFormat dateFormat1 = new SimpleDateFormat("yyyy-MM-dd 00:00:00");
        Session session = sessionFactory.getCurrentSession();
        Query query = session.createQuery("SELECT SUM(price) FROM TransactionHistory WHERE personId = "+ personId +" AND balance = 'Coperate' AND date between '"+ dateFormat1.format(start) +"' AND '"+ dateFormat.format(end) +"'");
        long sum = 0;
        if(query.getSingleResult()!=null)
        sum = (long) query.getSingleResult();
        return sum;
    }

    @Transactional(propagation = Propagation.REQUIRED)
    public long getSumPersonLiters(long personId, Date start, Date end) {
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd 23:59:59");
        SimpleDateFormat dateFormat1 = new SimpleDateFormat("yyyy-MM-dd 00:00:00");
        Session session = sessionFactory.getCurrentSession();
        Query query = session.createQuery("SELECT SUM(priceLiter) FROM TransactionHistory WHERE personId = "+ personId +" AND balance = 'Coperate' AND date between '"+ dateFormat1.format(start) +"' AND '"+ dateFormat.format(end) +"'");
        long sum = 0;
        if(query.getSingleResult()!=null)
            sum = (long) query.getSingleResult();
        return sum;
    }

    @Transactional(propagation = Propagation.REQUIRED)
    public long getPersonProfitByGase(long personId, String gas, Date start, Date end) {
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd 23:59:59");
        SimpleDateFormat dateFormat1 = new SimpleDateFormat("yyyy-MM-dd 00:00:00");
        Session session = sessionFactory.getCurrentSession();
        Query query = session.createQuery("SELECT SUM(price) FROM TransactionHistory WHERE personId = "+ personId +" AND gas = '"+ gas +"' AND balance = 'Coperate' AND date between '"+ dateFormat1.format(start) +"' AND '"+ dateFormat.format(end) +"'");
        long sum = 0;
        if(query.getSingleResult()!=null)
            sum = (long) query.getSingleResult();
        return sum;
    }

    @Transactional(propagation = Propagation.REQUIRED)
    public long getPersonLitersByGase(long personId, String gas, Date start, Date end) {
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd 23:59:59");
        SimpleDateFormat dateFormat1 = new SimpleDateFormat("yyyy-MM-dd 00:00:00");
        Session session = sessionFactory.getCurrentSession();
        Query query = session.createQuery("SELECT SUM(priceLiter) FROM TransactionHistory WHERE personId = "+ personId +" AND gas = '"+ gas +"' AND balance = 'Coperate' AND date between '"+ dateFormat1.format(start) +"' AND '"+ dateFormat.format(end) +"'");
        long sum = 0;
        if(query.getSingleResult()!=null)
            sum = (long) query.getSingleResult();
        return sum;
    }

    @Transactional(propagation = Propagation.REQUIRED)
    public List<TransactionHistory> getSumPersonGases(long personId, Date start, Date end) {
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd 23:59:59");
        SimpleDateFormat dateFormat1 = new SimpleDateFormat("yyyy-MM-dd 00:00:00");
        Session session = sessionFactory.getCurrentSession();
        Query query = session.createQuery("SELECT DISTINCT gas FROM TransactionHistory t WHERE personId = "+ personId +" AND balance = 'Coperate' AND date between '"+ dateFormat1.format(start) +"' AND '"+ dateFormat.format(end) +"'");
        List<String> list = query.getResultList();
        List<TransactionHistory> histories = new ArrayList<>();
        for (String gas: list) {
            histories.add(new TransactionHistory(gas));
        }
        return histories;
    }

    @Transactional(propagation = Propagation.REQUIRED)
    public long getProfitByGase(long companyId, String gas, Date start, Date end) {
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd 23:59:59");
        SimpleDateFormat dateFormat1 = new SimpleDateFormat("yyyy-MM-dd 00:00:00");
        Session session = sessionFactory.getCurrentSession();
        Query query = session.createQuery("SELECT SUM(t.price) FROM TransactionHistory t WHERE t.departmentName = (SELECT d.name FROM Department d WHERE d.company = " + companyId + ") AND t.gas = '"+ gas +"' AND t.balance = 'Coperate' AND t.date between '"+ dateFormat1.format(start) +"' AND '"+ dateFormat.format(end) +"'");
        long sum = 0;
        if(query.getSingleResult()!=null)
            sum = (long) query.getSingleResult();
        return sum;
    }

    @Transactional(propagation = Propagation.REQUIRED)
    public long getLitersByGase(long companyId, String gas, Date start, Date end) {
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd 23:59:59");
        SimpleDateFormat dateFormat1 = new SimpleDateFormat("yyyy-MM-dd 00:00:00");
        Session session = sessionFactory.getCurrentSession();
        Query query = session.createQuery("SELECT SUM(t.priceLiter) FROM TransactionHistory t WHERE t.departmentName = (SELECT d.name FROM Department d WHERE d.company = " + companyId + ") AND t.gas = '"+ gas +"' AND t.balance = 'Coperate' AND t.date between '"+ dateFormat1.format(start) +"' AND '"+ dateFormat.format(end) +"'");
        long sum = 0;
        if(query.getSingleResult()!=null)
            sum = (long) query.getSingleResult();
        return sum;
    }

    @Transactional(propagation = Propagation.REQUIRED)
    public List<Gas> getSumGases(long companyId, Date start, Date end) {
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd 23:59:59");
        SimpleDateFormat dateFormat1 = new SimpleDateFormat("yyyy-MM-dd 00:00:00");
        Session session = sessionFactory.getCurrentSession();
        Query query = session.createQuery("SELECT DISTINCT t.gas FROM TransactionHistory t WHERE t.departmentName = (SELECT d.name FROM Department d WHERE d.company = " + companyId + ") AND t.balance = 'Coperate' AND t.date between '"+ dateFormat1.format(start) +"' AND '"+ dateFormat.format(end) +"'");
        List<String> list = query.getResultList();
        List<Gas> gases = new ArrayList<>();
        for (String gas: list) {
            gases.add(new Gas(gas));
        }
        return gases;
    }

    @Transactional(propagation = Propagation.REQUIRED)
    public List<Person> getPersonListByCompanyId(long companyId){
        Session session = sessionFactory.getCurrentSession();
        Query query = session.createQuery("from Person where departmentId = ANY(SELECT departmentId FROM Department WHERE company = " + companyId + ")");
        List<Person> list = query.getResultList();
        return list;
    }

    @Transactional(propagation = Propagation.REQUIRED)
    public List<Person> searchStationHeadByNameAndSurname(long companyId, String name, String surname) {
        Session session = sessionFactory.getCurrentSession();
        Query query = session.createQuery("FROM Person p WHERE (p.personId NOT IN (SELECT r.personId FROM Role r WHERE r.role LIKE '%ROLE_WORKER%' OR r.role LIKE '%ROLE_HEAD%' OR r.role LIKE '%ROLE_DIRECTOR%' OR r.role LIKE '%ROLE_CASHIER%') OR (p.departmentId IN (SELECT d.departmentId FROM Department d WHERE d.company = " + companyId + ") AND p.personId IN (SELECT r.personId FROM Role r WHERE r.role LIKE '%ROLE_CASHIER%'))) AND p.name LIKE '%" + name + "%' AND p.surname LIKE '%" + surname + "%'");
        List<Person> list = query.getResultList();
        return list;
    }

    @Transactional(propagation = Propagation.REQUIRED)
    public List<Person> searchStationHeadByMail(long companyId, String pattern) {
        Session session = sessionFactory.getCurrentSession();
        Query query = session.createQuery("FROM Person p WHERE (p.personId NOT IN (SELECT r.personId FROM Role r WHERE r.role LIKE '%ROLE_WORKER%' OR r.role LIKE '%ROLE_HEAD%' OR r.role LIKE '%ROLE_DIRECTOR%' OR r.role LIKE '%ROLE_CASHIER%') OR (p.departmentId IN (SELECT d.departmentId FROM Department d WHERE d.company = " + companyId + ") AND p.personId IN (SELECT r.personId FROM Role r WHERE r.role LIKE '%ROLE_CASHIER%'))) AND p.email LIKE '%" + pattern + "%'");
        List<Person> list = query.getResultList();
        return list;
    }

    @Transactional(propagation = Propagation.REQUIRED)
    public List<Person> searchStationHeadByPhone(long companyId, String pattern) {
        Session session = sessionFactory.getCurrentSession();
        Query query = session.createQuery("FROM Person p WHERE (p.personId NOT IN (SELECT r.personId FROM Role r WHERE r.role LIKE '%ROLE_WORKER%' OR r.role LIKE '%ROLE_HEAD%' OR r.role LIKE '%ROLE_DIRECTOR%' OR r.role LIKE '%ROLE_CASHIER%') OR (p.departmentId IN (SELECT d.departmentId FROM Department d WHERE d.company = " + companyId + ") AND p.personId IN (SELECT r.personId FROM Role r WHERE r.role LIKE '%ROLE_CASHIER%'))) AND p.phone LIKE '%" + pattern + "%'");
        List<Person> list = query.getResultList();
        return list;
    }

    @Transactional(propagation = Propagation.REQUIRED)
    public List<Person> searchStationHeadByPattern(long companyId, String pattern) {
        Session session = sessionFactory.getCurrentSession();
        Query query = session.createQuery("FROM Person p WHERE (p.personId NOT IN (SELECT r.personId FROM Role r WHERE r.role LIKE '%ROLE_WORKER%' OR r.role LIKE '%ROLE_HEAD%' OR r.role LIKE '%ROLE_DIRECTOR%' OR r.role LIKE '%ROLE_CASHIER%') OR (p.departmentId IN (SELECT d.departmentId FROM Department d WHERE d.company = " + companyId + ") AND p.personId IN (SELECT r.personId FROM Role r WHERE r.role LIKE '%ROLE_CASHIER%'))) AND p.login LIKE '%" + pattern + "%'");
        List<Person> list = query.getResultList();
        return list;
    }

    @Transactional(propagation = Propagation.REQUIRED)
    public List<Shift> getOpenShiftsByPersonId(long personId) {
        Session session = sessionFactory.getCurrentSession();
        Query query = session.createQuery("FROM Shift WHERE status = 'open' AND personId = " + personId);
        List<Shift> list = query.getResultList();
        return list;
    }

    @Transactional(propagation = Propagation.REQUIRED)
    public List<TransactionHistory> getTransactionsByDate(String startDate, String endDate) {
        Session session = sessionFactory.getCurrentSession();
        Query query = session.createQuery("FROM TransactionHistory date BETWEEN '" + startDate + "' AND '" + endDate + "' ORDER BY date DESC");
        List<TransactionHistory> list = query.getResultList();
        return list;
    }

    @Transactional(propagation = Propagation.REQUIRED)
    public List<TransactionHistory> getStationTransactionsByDate(Date startDate, Date endDate, long page) {
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd 23:59:59");
        SimpleDateFormat dateFormat1 = new SimpleDateFormat("yyyy-MM-dd 00:00:00");
        Session session = sessionFactory.getCurrentSession();
        Query query = session.createQuery("FROM TransactionHistory WHERE date BETWEEN '" + dateFormat1.format(startDate) + "' AND '" + dateFormat.format(endDate) + "' ORDER BY date DESC").setFirstResult((int) (10*(page-1))).setMaxResults(10);
        List<TransactionHistory> list = query.getResultList();
        return list;
    }

    public List<Department> getStationList() {
        Session session = sessionFactory.getCurrentSession();
        Query query = session.createQuery("from Department d where d.company = (select c.companyId from Compant c where c.name = 'Elf')");
        List<Department> list = query.getResultList();
        return list;
    }
}