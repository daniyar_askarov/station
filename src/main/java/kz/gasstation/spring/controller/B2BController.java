package kz.gasstation.spring.controller;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JavaType;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.module.SimpleModule;
import kz.gasstation.spring.dao.deserialize.*;
import kz.gasstation.spring.dao.entities.*;
import kz.gasstation.spring.dao.implementations.HibernateDao;
import kz.gasstation.spring.dao.serialize.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;

import java.io.IOException;
import java.util.Date;
import java.util.List;
import java.util.StringTokenizer;

@RestController
public class B2BController {

    @Autowired
    HibernateDao hibernateDao;

    @RequestMapping(value = "/b2b-director", method = RequestMethod.POST,produces = MediaType.APPLICATION_JSON_UTF8_VALUE, consumes=MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public String getB2Bdirector(@RequestBody String jsonString) throws JsonProcessingException, IOException {
        ObjectMapper mapper = new ObjectMapper();
        SimpleModule module = new SimpleModule();
        module.addDeserializer(Person.class, new B2BHomeDeserialize());
        mapper.registerModule(module);
        Person person = mapper.readValue(jsonString, Person.class);
        person = hibernateDao.getPersonById(person.getPersonId());
        person.setAccounts(hibernateDao.getAccountListByPersonId(person.getPersonId()));
        Company company = hibernateDao.getCompanyByHeadId(person.getPersonId());
        List<Department> departmentList = hibernateDao.getDepartmentListByCompanyId(company.getCompanyId());
        for (Department department: departmentList) {
            department.setHead(hibernateDao.getHeadByDepartmentId(department.getDepartmentId()));
        }
        company.setDepartmentList(departmentList);
        person.setCompany(company);
        module.addSerializer(Person.class, new B2BDirectorSerialize());
        mapper.registerModule(module);
        String json = mapper.writeValueAsString(person);
        return json;
    }

    @RequestMapping(value = "/b2b-head", method = RequestMethod.POST,produces = MediaType.APPLICATION_JSON_UTF8_VALUE, consumes=MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public String getB2Bhead(@RequestBody String jsonString) throws JsonProcessingException, IOException {
        ObjectMapper mapper = new ObjectMapper();
        SimpleModule module = new SimpleModule();
        module.addDeserializer(Person.class, new B2BHomeDeserialize());
        mapper.registerModule(module);
        Person person = mapper.readValue(jsonString, Person.class);
        person = hibernateDao.getPersonById(person.getPersonId());
        person.setAccounts(hibernateDao.getAccountListByPersonId(person.getPersonId()));
        Department department = hibernateDao.getDepartmentByHeadId(person.getPersonId());
        List<Person> personList = hibernateDao.getPersonListByDepartmentId(person.getPersonId(),department.getDepartmentId());
        department.setNumberOfstaff(hibernateDao.getCountOfPersons(person.getPersonId(),department.getDepartmentId()));
        department.setPersonList(personList);
        person.setDep(department);
        Company company = hibernateDao.getCompanyByDepartmentId(department.getDepartmentId());
        person.setCompany(company);
        module.addSerializer(Person.class, new B2BHeadSerialize());
        mapper.registerModule(module);
        String json = mapper.writeValueAsString(person);
        return json;
    }

    @RequestMapping(value = "/getdepartment", method = RequestMethod.POST,produces = MediaType.APPLICATION_JSON_UTF8_VALUE, consumes=MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public String getDepartment(@RequestBody String jsonString) throws JsonProcessingException, IOException {
        ObjectMapper mapper = new ObjectMapper();
        SimpleModule module = new SimpleModule();
        module.addDeserializer(Department.class, new GetDepartmentDeserialize());
        mapper.registerModule(module);
        Department department = mapper.readValue(jsonString, Department.class);
        Person head = hibernateDao.getHeadByDepartmentId(department.getDepartmentId());
        head.setAccounts(hibernateDao.getAccountListByPersonId(head.getPersonId()));
        department = hibernateDao.getDepartmentById(department.getDepartmentId());
        List<Person> personList = hibernateDao.getPersonListByDepartmentId(head.getPersonId(),department.getDepartmentId());
        for (Person person: personList) {
            person.setAccounts(hibernateDao.getAccountListByPersonId(person.getPersonId()));
        }
        department.setNumberOfstaff(hibernateDao.getCountOfPersons(head.getPersonId(),department.getDepartmentId()));
        department.setPersonList(personList);
        head.setDep(department);
        Company company = hibernateDao.getCompanyByDepartmentId(department.getDepartmentId());
        head.setCompany(company);
        module.addSerializer(Person.class, new B2BHeadSerialize());
        mapper.registerModule(module);
        String json = mapper.writeValueAsString(head);
        return json;
    }

    @RequestMapping(value = "/create-department", method = RequestMethod.POST,produces = MediaType.APPLICATION_JSON_UTF8_VALUE, consumes=MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public String createDepartment(@RequestBody String jsonString) throws JsonProcessingException, IOException {
        ObjectMapper mapper = new ObjectMapper();
        SimpleModule module = new SimpleModule();
        module.addDeserializer(Department.class, new DepartmentCreationDeserialize());
        mapper.registerModule(module);
        Department department = mapper.readValue(jsonString, Department.class);
        Person person = hibernateDao.getPersonById(department.getHead().getPersonId());
        department.setHead(person);
        hibernateDao.createDepartment(department);
        department = hibernateDao.getDepartmentByHeadId(department.getHead().getPersonId());
        person.setDepartmentId(department);
        Role role = person.getRole();
        role.setRole(role.getRole() + ", ROLE_HEAD");
        Account account = new Account("Coperate",0,person);
        person.setActiveAccount(account);
        hibernateDao.updateRole(role);
        hibernateDao.mergePerson(person);
        return "true";
    }

    @RequestMapping(value = "/update-department", method = RequestMethod.POST,produces = MediaType.APPLICATION_JSON_UTF8_VALUE, consumes=MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public String updateDepartment(@RequestBody String jsonString) throws JsonProcessingException, IOException {
        ObjectMapper mapper = new ObjectMapper();
        SimpleModule module = new SimpleModule();
        module.addDeserializer(Department.class, new DepartmentUpdateDeserialize());
        mapper.registerModule(module);
        Department department = mapper.readValue(jsonString, Department.class);
        String departmentName = department.getName();
        Person person = hibernateDao.getPersonById(department.getHead().getPersonId());
        department = hibernateDao.getDepartmentById(department.getDepartmentId());
        department.setName(departmentName);
        Person oldHead = hibernateDao.getHeadByDepartmentId(department.getDepartmentId());
        person.setDepartmentId(department);
        Role role = person.getRole();
        if(role.getRole().contains(", ROLE_WORKER")){
            role.setRole(role.getRole().replaceAll(", ROLE_WORKER",""));
        }
        if(!role.getRole().contains(", ROLE_HEAD")) {
            role.setRole(role.getRole() + ", ROLE_HEAD");
        }
        boolean fl = false;
        for (Account account: person.getAccounts()) {
            if (account.getName().equals("Coperate")) fl = true;
        }
        if(fl == false){
        Account account = new Account("Coperate",0,person);
        List<Account> accountList = person.getAccounts();
        accountList.add(account);
        person.setAccounts(accountList);}
        hibernateDao.updateRole(role);
        if(oldHead.getPersonId() != person.getPersonId()) department.setHead(person);
        hibernateDao.updateDepartment(department);


        if(oldHead.getPersonId() == person.getPersonId()) return "true";
        oldHead.setDepartmentId(null);
        role = oldHead.getRole();
        role.setRole(role.getRole().replaceAll(", ROLE_HEAD",""));
        List<Account>accountList = oldHead.getAccounts();
        Account acc = null;
        for (Account account: accountList) {
            if(account.getName().equals("Personal")){
                oldHead.setActiveAccount(account);
            }else{
                acc = account;
            }
        }
        for (Account account: person.getAccounts()) {
            if(account.getName().equals("Coperate")) {
                account.setBalance(account.getBalance() + acc.getBalance());
                hibernateDao.updateAccountBalance(account);
            }
        }
        accountList.remove(acc);
        oldHead.setAccounts(accountList);
        hibernateDao.deleteAccount(acc);
        hibernateDao.updateRole(role);
        hibernateDao.mergePerson(oldHead);

        return "true";
    }


    @RequestMapping(value = "/delete-department", method = RequestMethod.POST,produces = MediaType.APPLICATION_JSON_UTF8_VALUE, consumes=MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public String deleteDepartment(@RequestBody String jsonString) throws JsonProcessingException, IOException {
        ObjectMapper mapper = new ObjectMapper();
        SimpleModule module = new SimpleModule();
        module.addDeserializer(Department.class, new DepartmentDeleteDeserialize());
        mapper.registerModule(module);
        Department department = mapper.readValue(jsonString, Department.class);
        department = hibernateDao.getDepartmentById(department.getDepartmentId());
        Person head = hibernateDao.getHeadByDepartmentId(department.getDepartmentId());
        List<Person> personList = hibernateDao.getPersonListByDepartmentId(head.getPersonId(),department.getDepartmentId());
        Person director = hibernateDao.getDirectorByDepartmentId(department.getDepartmentId());
        for (Person person: personList) {
            person.setDepartmentId(null);
            Role role = person.getRole();
            role.setRole(role.getRole().replaceAll(", ROLE_WORKER",""));
            List<Account> accountList = person.getAccounts();
            Account acc = null;
            for (Account account: accountList) {
                if(account.getName().equals("Personal")){
                    person.setActiveAccount(account);
                }else{
                    acc = account;
                }
            }
            for (Account account: head.getAccounts()) {
                if(account.getName().equals("Coperate")) {
                    account.setBalance(account.getBalance() + acc.getBalance());
                    hibernateDao.updateAccountBalance(account);
                }
            }
            accountList.remove(acc);
            person.setAccounts(accountList);
            hibernateDao.deleteAccount(acc);
            hibernateDao.updateRole(role);
            hibernateDao.mergePerson(person);
        }

        head.setDepartmentId(null);
        Role role = head.getRole();
        role.setRole(role.getRole().replaceAll(", ROLE_HEAD",""));
        List<Account> accountList = head.getAccounts();
        Account acc = null;
        for (Account account: accountList) {
            if(account.getName().equals("Personal")){
                head.setActiveAccount(account);
            }else{
                acc = account;
            }
        }
        for (Account account: director.getAccounts()) {
            if(account.getName().equals("Coperate")) {
                account.setBalance(account.getBalance() + acc.getBalance());
                hibernateDao.updateAccountBalance(account);
            }
        }
        accountList.remove(acc);
        head.setAccounts(accountList);
        hibernateDao.deleteAccount(acc);
        hibernateDao.updateRole(role);
        hibernateDao.mergePerson(head);

        hibernateDao.deleteDepartment(department);
        return "true";
    }

    @RequestMapping(value = "/addcooperator", method = RequestMethod.POST,produces = MediaType.APPLICATION_JSON_UTF8_VALUE, consumes=MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public String addCooperator(@RequestBody String jsonString) throws JsonProcessingException, IOException {
        ObjectMapper mapper = new ObjectMapper();
        SimpleModule module = new SimpleModule();
        module.addDeserializer(Person.class, new AddCooperatorDeserialize());
        mapper.registerModule(module);
        Person person = mapper.readValue(jsonString, Person.class);
        Department department = hibernateDao.getDepartmentById(person.getDepartmentId().getDepartmentId());
        person = hibernateDao.getPersonById(person.getPersonId());
        person.setDepartmentId(department);
        Role role = person.getRole();
        role.setRole(role.getRole() + ", ROLE_WORKER");
        Account account = new Account("Coperate",0,person);
        person.setActiveAccount(account);
        hibernateDao.updateRole(role);
        hibernateDao.mergePerson(person);
        return "true";
    }

    @RequestMapping(value = "/deletecooperator", method = RequestMethod.POST,produces = MediaType.APPLICATION_JSON_UTF8_VALUE, consumes=MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public String deleteCooperator(@RequestBody String jsonString) throws JsonProcessingException, IOException {
        ObjectMapper mapper = new ObjectMapper();
        SimpleModule module = new SimpleModule();
        module.addDeserializer(Person.class, new AddCooperatorDeserialize());
        mapper.registerModule(module);
        Person person = mapper.readValue(jsonString, Person.class);
        Person head = hibernateDao.getHeadByDepartmentId(person.getDepartmentId().getDepartmentId());
        person = hibernateDao.getPersonById(person.getPersonId());
        person.setDepartmentId(null);
        Role role = person.getRole();
        role.setRole(role.getRole().replaceAll(", ROLE_WORKER",""));
        List<Account> accountList = person.getAccounts();
        Account acc = null;
        for (Account account: accountList) {
            if(account.getName().equals("Personal")){
                person.setActiveAccount(account);
            }else{
                acc = account;
            }
        }
        for (Account account: head.getAccounts()) {
            if(account.getName().equals("Coperate")) {
                account.setBalance(account.getBalance() + acc.getBalance());
                hibernateDao.updateAccountBalance(account);
            }
        }
        accountList.remove(acc);
        person.setAccounts(accountList);
        hibernateDao.deleteAccount(acc);
        hibernateDao.updateRole(role);
        hibernateDao.mergePerson(person);
        return "true";
    }

    @RequestMapping(value = "/searchperson", method = RequestMethod.POST,produces = MediaType.APPLICATION_JSON_UTF8_VALUE, consumes=MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public String searchPerson(@RequestBody String jsonString) throws JsonProcessingException, IOException {
        ObjectMapper mapper = new ObjectMapper();
        SimpleModule module = new SimpleModule();
        module.addDeserializer(Person.class, new PersonSearchDeserialize());
        mapper.registerModule(module);
        Person person = mapper.readValue(jsonString, Person.class);
        StringTokenizer st = new StringTokenizer(person.getLogin()," ");
        String pattern = st.nextToken();
        List<Person> personList;
        if(st.hasMoreTokens()){
            String pattern02 = st.nextToken();
            personList = hibernateDao.searchPersonsByNameAndSurname(pattern,pattern02);
        } else if(pattern.contains("@")){
            personList = hibernateDao.searchPersonsByMail(pattern);
        } else if(pattern.charAt(0) == '+' || pattern.charAt(0) == '8' || pattern.charAt(0) == '7') {
            personList = hibernateDao.searchPersonsByPhone(pattern);
        } else {
            personList = hibernateDao.searchPersonsByPattern(pattern);
        }
        module.addSerializer((Class)List.class, new PersonSearchSerialize());
        mapper.registerModule(module);
        String json = mapper.writeValueAsString(personList);
        return json;
    }

    @RequestMapping(value = "/searchcooperator", method = RequestMethod.POST,produces = MediaType.APPLICATION_JSON_UTF8_VALUE, consumes=MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public String searchCooperator(@RequestBody String jsonString) throws JsonProcessingException, IOException {
        ObjectMapper mapper = new ObjectMapper();
        SimpleModule module = new SimpleModule();
        module.addDeserializer(Person.class, new CooperatorSearchDeserialize());
        mapper.registerModule(module);
        Person person = mapper.readValue(jsonString, Person.class);
        StringTokenizer st = new StringTokenizer(person.getLogin()," ");
        String pattern = st.nextToken();
        List<Person> personList;
        if(st.hasMoreTokens()){
            String pattern02 = st.nextToken();
            personList = hibernateDao.getPersonListByNameAndSurname(person.getDepartmentId().getDepartmentId(),pattern,pattern02);
        } else if(pattern.contains("@")){
            personList = hibernateDao.searchCooperateByMail(person.getDepartmentId().getDepartmentId(),pattern);
        } else if(pattern.charAt(0) == '+' || pattern.charAt(0) == '8' || pattern.charAt(0) == '7') {
            personList = hibernateDao.searchCooperateByPhone(person.getDepartmentId().getDepartmentId(),pattern);
        } else {
            personList = hibernateDao.searchCooperateByName(person.getDepartmentId().getDepartmentId(),pattern);
        }
        module.addSerializer((Class)List.class, new PersonSearchSerialize());
        mapper.registerModule(module);
        String json = mapper.writeValueAsString(personList);
        return json;
    }

    @RequestMapping(value = "/setbalance", method = RequestMethod.POST,produces = MediaType.APPLICATION_JSON_UTF8_VALUE, consumes=MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public String exchangeBalance(@RequestBody String jsonString) throws JsonProcessingException, IOException {
        ObjectMapper mapper = new ObjectMapper();
        SimpleModule module = new SimpleModule();
        module.addDeserializer(Person.class, new ExchangeBalanceDeserialize());
        mapper.registerModule(module);
        JavaType collection = mapper.getTypeFactory().constructCollectionType(List.class, Person.class);
        List<Person> personList = (List<Person>)mapper.readValue(jsonString, collection);
        for (Person person : personList) {
            long fromPersonId = person.getDepartment().getDepartmentId();
            long price = person.getCompany().getCompanyId();
            Account toAccount = hibernateDao.getCoperateBalance(person.getPersonId());
            Account fromAccount = hibernateDao.getCoperateBalance(fromPersonId);
            if (((fromAccount.getBalance() - price) >= 0) && ((toAccount.getBalance() + price) >= 0)) {
                toAccount.setBalance(toAccount.getBalance() + price);
                fromAccount.setBalance(fromAccount.getBalance() - price);
                hibernateDao.updateAccountBalance(toAccount);
                hibernateDao.updateAccountBalance(fromAccount);
            } else return "false";
        }
        return "true";
    }

    @RequestMapping(value = "/setsinglebalance", method = RequestMethod.POST,produces = MediaType.APPLICATION_JSON_UTF8_VALUE, consumes=MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public String exchangeSingleBalance(@RequestBody String jsonString) throws JsonProcessingException, IOException {
        ObjectMapper mapper = new ObjectMapper();
        SimpleModule module = new SimpleModule();
        module.addDeserializer(Person.class, new ExchangeBalanceDeserialize());
        mapper.registerModule(module);
        Person person = mapper.readValue(jsonString, Person.class);
        long fromPersonId = person.getDepartment().getDepartmentId();
        long price = person.getCompany().getCompanyId();
        Account toAccount = hibernateDao.getCoperateBalance(person.getPersonId());
        Account fromAccount = hibernateDao.getCoperateBalance(fromPersonId);
        if (((fromAccount.getBalance() - price) >= 0) && ((toAccount.getBalance() + price) >= 0)) {
            toAccount.setBalance(toAccount.getBalance() + price);
            fromAccount.setBalance(fromAccount.getBalance() - price);
            hibernateDao.updateAccountBalance(toAccount);
            hibernateDao.updateAccountBalance(fromAccount);
        } else return "false";
        return "true";
    }

    @RequestMapping(value = "/getcooptransactions", method = RequestMethod.POST,produces = MediaType.APPLICATION_JSON_UTF8_VALUE, consumes=MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public String getCooperatorTransactions(@RequestBody String jsonString) throws JsonProcessingException, IOException {
        ObjectMapper mapper = new ObjectMapper();
        SimpleModule module = new SimpleModule();
        module.addDeserializer(Person.class, new TransactionsDeserialize());
        mapper.registerModule(module);
        Person person = mapper.readValue(jsonString, Person.class);
        List<TransactionHistory> transactionHistories = person.getTransactionHistories();
        Date startDate = transactionHistories.get(0).getDate();
        Date endDate = transactionHistories.get(1).getDate();
        transactionHistories = hibernateDao.getCooperatorTransactions(person.getPersonId(),startDate,endDate);
        if(transactionHistories != null){
            TransactionHistory transactionHistory = new TransactionHistory();
            transactionHistory.setPrice(hibernateDao.getCountOfTransactions(person.getPersonId(),"Coperate",startDate,endDate));
            transactionHistories.add(transactionHistory);
        }
        module.addSerializer((Class)List.class, new THistorySerialize());
        mapper.registerModule(module);
        String json = mapper.writeValueAsString(transactionHistories);
        return json;
    }

    @RequestMapping(value = "/getpersonaltransactions", method = RequestMethod.POST,produces = MediaType.APPLICATION_JSON_UTF8_VALUE, consumes=MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public String getPersonalTransactions(@RequestBody String jsonString) throws JsonProcessingException, IOException {
        ObjectMapper mapper = new ObjectMapper();
        SimpleModule module = new SimpleModule();
        module.addDeserializer(Person.class, new TransactionsDeserialize());
        mapper.registerModule(module);
        Person person = mapper.readValue(jsonString, Person.class);
        List<TransactionHistory> transactionHistories = person.getTransactionHistories();
        Date startDate = transactionHistories.get(0).getDate();
        Date endDate = transactionHistories.get(1).getDate();
        transactionHistories = hibernateDao.getPersonalTransactions(person.getPersonId(),startDate,endDate);
        if(transactionHistories != null){
            TransactionHistory transactionHistory = new TransactionHistory();
            transactionHistory.setPrice(hibernateDao.getCountOfTransactions(person.getPersonId(),"Personal",startDate,endDate));
            transactionHistories.add(transactionHistory);
        }
        module.addSerializer((Class)List.class, new THistorySerialize());
        mapper.registerModule(module);
        String json = mapper.writeValueAsString(transactionHistories);
        return json;
    }


    @RequestMapping(value = "/searchhead", method = RequestMethod.POST,produces = MediaType.APPLICATION_JSON_UTF8_VALUE, consumes=MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public String searchHead(@RequestBody String jsonString) throws JsonProcessingException, IOException {
        ObjectMapper mapper = new ObjectMapper();
        SimpleModule module = new SimpleModule();
        module.addDeserializer(Person.class, new CooperatorSearchDeserialize());
        mapper.registerModule(module);
        Person person = mapper.readValue(jsonString, Person.class);
        StringTokenizer st = new StringTokenizer(person.getLogin()," ");
        String pattern = st.nextToken();
        List<Person> personList;
        if(st.hasMoreTokens()){
            String pattern02 = st.nextToken();
            personList = hibernateDao.searchHeadByNameAndSurname(person.getDepartmentId().getDepartmentId(),pattern,pattern02);
        } else if(pattern.contains("@")){
            personList = hibernateDao.searchHeadByMail(person.getDepartmentId().getDepartmentId(),pattern);
        } else if(pattern.charAt(0) == '+' || pattern.charAt(0) == '8' || pattern.charAt(0) == '7') {
            personList = hibernateDao.searchHeadByPhone(person.getDepartmentId().getDepartmentId(),pattern);
        } else {
            personList = hibernateDao.searchHeadByPattern(person.getDepartmentId().getDepartmentId(),pattern);
        }
        module.addSerializer((Class)List.class, new PersonSearchSerialize());
        mapper.registerModule(module);
        String json = mapper.writeValueAsString(personList);
        return json;
    }


    @RequestMapping(value = "/searchintocompany", method = RequestMethod.POST,produces = MediaType.APPLICATION_JSON_UTF8_VALUE, consumes=MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public String searchIntoCompany(@RequestBody String jsonString) throws JsonProcessingException, IOException {
        ObjectMapper mapper = new ObjectMapper();
        SimpleModule module = new SimpleModule();
        module.addDeserializer(Person.class, new CooperatorSearchDeserialize());
        mapper.registerModule(module);
        Person person = mapper.readValue(jsonString, Person.class);
        StringTokenizer st = new StringTokenizer(person.getLogin()," ");
        String pattern = st.nextToken();
        List<Person> personList;
        if(st.hasMoreTokens()){
            String pattern02 = st.nextToken();
            personList = hibernateDao.searchIntoCompanyByNameAndSurname(person.getDepartmentId().getDepartmentId(),pattern,pattern02);
        } else if(pattern.contains("@")){
            personList = hibernateDao.searchIntoCompanyByMail(person.getDepartmentId().getDepartmentId(),pattern);
        } else if(pattern.charAt(0) == '+' || pattern.charAt(0) == '8' || pattern.charAt(0) == '7') {
            personList = hibernateDao.searchIntoCompanyByPhone(person.getDepartmentId().getDepartmentId(),pattern);
        } else {
            personList = hibernateDao.searchIntoCompanyByPattern(person.getDepartmentId().getDepartmentId(),pattern);
        }
        module.addSerializer((Class)List.class, new PersonSearchSerialize());
        mapper.registerModule(module);
        String json = mapper.writeValueAsString(personList);
        return json;
    }

}
