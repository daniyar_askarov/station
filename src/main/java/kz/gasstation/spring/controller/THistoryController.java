package kz.gasstation.spring.controller;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.module.SimpleModule;
import kz.gasstation.spring.dao.deserialize.B2BHomeDeserialize;
import kz.gasstation.spring.dao.deserialize.PageTransactionsDeserialize;
import kz.gasstation.spring.dao.deserialize.TransactionsDeserialize;
import kz.gasstation.spring.dao.entities.Person;
import kz.gasstation.spring.dao.entities.TransactionHistory;
import kz.gasstation.spring.dao.implementations.HibernateDao;
import kz.gasstation.spring.dao.serialize.THistorySerialize;
import kz.gasstation.spring.dao.serialize.TransactionSerialize;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;

import java.io.IOException;
import java.util.Date;
import java.util.List;

@RestController
public class THistoryController {

    @Autowired
    HibernateDao hibernateDao;

    @RequestMapping(value = "/gettransactions", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_UTF8_VALUE, consumes=MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public String getTransactions(@RequestBody String jsonString) throws IOException,JsonProcessingException {
        ObjectMapper mapper = new ObjectMapper();
        SimpleModule module = new SimpleModule();
        module.addDeserializer(Person.class, new PageTransactionsDeserialize());
        mapper.registerModule(module);
        Person person = mapper.readValue(jsonString, Person.class);
        List<TransactionHistory> list = person.getTransactionHistories();
        Date startDate = list.get(0).getDate();
        Date endDate = list.get(1).getDate();
        list = hibernateDao.getTransactionsByPersonId(person.getPersonId(),person.getDepartment().getDepartmentId(),startDate,endDate);
        if(list != null){
            TransactionHistory transactionHistory = new TransactionHistory();
            transactionHistory.setPrice(hibernateDao.getCountOfTransactions(person.getPersonId(),startDate,endDate));
            list.add(transactionHistory);
        }
        module.addSerializer((Class)List.class, new THistorySerialize());
        mapper.registerModule(module);
        String json = mapper.writeValueAsString(list);
        return json;
    }

    @RequestMapping(value = "/getprofittransactions", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_UTF8_VALUE, consumes=MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public String getProfitTransactions(@RequestBody String jsonString) throws IOException,JsonProcessingException {
        ObjectMapper mapper = new ObjectMapper();
        SimpleModule module = new SimpleModule();
        module.addDeserializer(Person.class, new TransactionsDeserialize());
        mapper.registerModule(module);
        Person person = mapper.readValue(jsonString, Person.class);
        List<TransactionHistory> list = person.getTransactionHistories();
        Date startDate = list.get(0).getDate();
        Date endDate = list.get(1).getDate();
        list = hibernateDao.getCooperatorTransactions(person.getPersonId(),startDate,endDate);
        module.addSerializer((Class)List.class, new TransactionSerialize());
        mapper.registerModule(module);
        String json = mapper.writeValueAsString(list);
        return json;
    }

    @RequestMapping(value = "/gethistories", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_UTF8_VALUE, consumes=MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public String getTransactionsByPersonId(@RequestBody String jsonString) throws IOException,JsonProcessingException {
        ObjectMapper mapper = new ObjectMapper();
        SimpleModule module = new SimpleModule();
        module.addDeserializer(Person.class, new B2BHomeDeserialize());
        mapper.registerModule(module);
        Person person = mapper.readValue(jsonString, Person.class);
        List<TransactionHistory> list = person.getTransactionHistories();
        Date startDate = list.get(0).getDate();
        Date endDate = list.get(1).getDate();
        list = hibernateDao.getTransactionsByPersonId(person.getPersonId(),startDate,endDate);
        if(list != null){
            TransactionHistory transactionHistory = new TransactionHistory();
            transactionHistory.setPrice(hibernateDao.getCountOfTransactions(person.getPersonId(),startDate,endDate));
            list.add(transactionHistory);
        }
        module.addSerializer((Class)List.class, new THistorySerialize());
        mapper.registerModule(module);
        String json = mapper.writeValueAsString(list);
        return json;
    }


}
