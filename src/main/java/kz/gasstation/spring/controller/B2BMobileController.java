package kz.gasstation.spring.controller;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.module.SimpleModule;
import kz.gasstation.spring.dao.deserialize.*;
import kz.gasstation.spring.dao.entities.Account;
import kz.gasstation.spring.dao.entities.Department;
import kz.gasstation.spring.dao.entities.Person;
import kz.gasstation.spring.dao.entities.Role;
import kz.gasstation.spring.dao.implementations.HibernateDao;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;

import java.io.IOException;
import java.util.List;

@RestController
public class B2BMobileController {

    @Autowired
    HibernateDao hibernateDao;

    @RequestMapping(value = "/createdepartment", method = RequestMethod.POST,produces = MediaType.APPLICATION_JSON_UTF8_VALUE, consumes=MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public String createDepartment(@RequestBody String jsonString) throws JsonProcessingException, IOException {
        ObjectMapper mapper = new ObjectMapper();
        SimpleModule module = new SimpleModule();
        module.addDeserializer(Department.class, new MDepartmentCreationDeserialize());
        mapper.registerModule(module);
        Department department = mapper.readValue(jsonString, Department.class);
        Person person = hibernateDao.getPersonByLogin(department.getHead().getLogin());
        department.setHead(person);
        hibernateDao.createDepartment(department);
        department = hibernateDao.getDepartmentByHeadId(department.getHead().getPersonId());
        person.setDepartmentId(department);
        Role role = person.getRole();
        role.setRole(role.getRole() + ", ROLE_HEAD");
        Account account = new Account("Coperate",0,person);
        person.setActiveAccount(account);
        hibernateDao.updateRole(role);
        hibernateDao.mergePerson(person);
        return "true";
    }

    @RequestMapping(value = "/updatedepartment", method = RequestMethod.POST,produces = MediaType.APPLICATION_JSON_UTF8_VALUE, consumes=MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public String updateDepartment(@RequestBody String jsonString) throws JsonProcessingException, IOException {
        ObjectMapper mapper = new ObjectMapper();
        SimpleModule module = new SimpleModule();
        module.addDeserializer(Department.class, new MDepartmentUpdateDeserialize());
        mapper.registerModule(module);
        Department department = mapper.readValue(jsonString, Department.class);
        String departmentName = department.getName();
        Person person = hibernateDao.getPersonByLogin(department.getHead().getLogin());
        department = hibernateDao.getDepartmentById(department.getDepartmentId());
        department.setName(departmentName);
        Person oldHead = hibernateDao.getHeadByDepartmentId(department.getDepartmentId());
        person.setDepartmentId(department);
        Role role = person.getRole();
        if(role.getRole().contains(", ROLE_WORKER")) {
            role.setRole(role.getRole().replaceAll(", ROLE_WORKER", ""));
        }
        if(!role.getRole().contains(", ROLE_HEAD")) {
            role.setRole(role.getRole() + ", ROLE_HEAD");
        }
        boolean fl = false;
        for (Account account: person.getAccounts()) {
            if (account.getName().equals("Coperate")) fl = true;
        }
        if(fl == false){
            Account account = new Account("Coperate",0,person);
            List<Account> accountList = person.getAccounts();
            accountList.add(account);
            person.setAccounts(accountList);}
        hibernateDao.updateRole(role);
        if(oldHead.getPersonId() != person.getPersonId()) department.setHead(person);
        hibernateDao.updateDepartment(department);


        if(oldHead.getPersonId() == person.getPersonId()) return "true";
        oldHead.setDepartmentId(null);
        role = oldHead.getRole();
        role.setRole(role.getRole().replaceAll(", ROLE_HEAD",""));
        List<Account>accountList = oldHead.getAccounts();
        Account acc = null;
        for (Account account: accountList) {
            if(account.getName().equals("Personal")){
                oldHead.setActiveAccount(account);
            }else{
                acc = account;
            }
        }
        for (Account account: person.getAccounts()) {
            if(account.getName().equals("Coperate")) {
                account.setBalance(account.getBalance() + acc.getBalance());
                hibernateDao.updateAccountBalance(account);
            }
        }
        accountList.remove(acc);
        oldHead.setAccounts(accountList);
        hibernateDao.deleteAccount(acc);
        hibernateDao.updateRole(role);
        hibernateDao.mergePerson(oldHead);

        return "true";
    }

    @RequestMapping(value = "/add-cooperator", method = RequestMethod.POST,produces = MediaType.APPLICATION_JSON_UTF8_VALUE, consumes=MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public String addCooperator(@RequestBody String jsonString) throws JsonProcessingException, IOException {
        ObjectMapper mapper = new ObjectMapper();
        SimpleModule module = new SimpleModule();
        module.addDeserializer(Person.class, new MAddCooperatorDeserialize());
        mapper.registerModule(module);
        Person person = mapper.readValue(jsonString, Person.class);
        Department department = hibernateDao.getDepartmentById(person.getDepartmentId().getDepartmentId());
        person = hibernateDao.getPersonByLogin(person.getLogin());
        person.setDepartmentId(department);
        Role role = person.getRole();
        role.setRole(role.getRole() + ", ROLE_WORKER");
        Account account = new Account("Coperate",0,person);
        person.setActiveAccount(account);
        hibernateDao.updateRole(role);
        hibernateDao.mergePerson(person);
        return "true";
    }

}
