package kz.gasstation.spring.controller;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.module.SimpleModule;
import kz.gasstation.spring.dao.deserialize.B2BHomeDeserialize;
import kz.gasstation.spring.dao.entities.Account;
import kz.gasstation.spring.dao.entities.Person;
import kz.gasstation.spring.dao.implementations.HibernateDao;
import kz.gasstation.spring.dao.serialize.BalanceSerialize;
import kz.gasstation.spring.dao.serialize.PersonSearchSerialize;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;

import java.io.IOException;
import java.util.List;

@RestController
public class AccountController {

    @Autowired
    HibernateDao hibernateDao;

    @RequestMapping(value = "/getbalances", method = RequestMethod.POST,produces = MediaType.APPLICATION_JSON_UTF8_VALUE, consumes=MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public String getB2Bdirector(@RequestBody String jsonString) throws JsonProcessingException, IOException {
        ObjectMapper mapper = new ObjectMapper();
        SimpleModule module = new SimpleModule();
        module.addDeserializer(Person.class, new B2BHomeDeserialize());
        mapper.registerModule(module);
        Person person = mapper.readValue(jsonString, Person.class);
        person = hibernateDao.getPersonById(person.getPersonId());
        module.addSerializer(Person.class, new BalanceSerialize());
        mapper.registerModule(module);
        String json = mapper.writeValueAsString(person);
        return json;
    }

}
