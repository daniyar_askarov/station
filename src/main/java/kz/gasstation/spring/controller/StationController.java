package kz.gasstation.spring.controller;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.module.SimpleModule;
import com.fasterxml.jackson.databind.node.ObjectNode;
import javafx.collections.transformation.SortedList;
import kz.gasstation.spring.dao.deserialize.*;
import kz.gasstation.spring.dao.entities.*;
import kz.gasstation.spring.dao.implementations.HibernateDao;
import kz.gasstation.spring.dao.serialize.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.web.bind.annotation.*;

import java.io.IOException;
import java.sql.SQLException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;

@RestController
public class StationController {

    @Autowired
    HibernateDao hibernateDao;

    @RequestMapping(value = "/getstationcompany", method = RequestMethod.POST,produces = MediaType.APPLICATION_JSON_UTF8_VALUE, consumes=MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public String getStationDirector(@RequestBody String jsonString) throws JsonProcessingException, IOException {
        ObjectMapper mapper = new ObjectMapper();
        SimpleModule module = new SimpleModule();
        module.addDeserializer(Person.class, new DashboardDeserialize());
        mapper.registerModule(module);
        Person person = mapper.readValue(jsonString, Person.class);
        person = hibernateDao.getPersonByLoginAndPassword(person.getLogin(),person.getPassword());
        if(person == null) return "false";
        if(person.getRole().getRole().contains("ROLE_DIRECTOR_STATION")){
            Company company = hibernateDao.getCompanyByHeadId(person.getPersonId());
            List<Department> departmentList = hibernateDao.getDepartmentListByCompanyId(company.getCompanyId());
            for (Department department: departmentList) {
                department.setHead(hibernateDao.getHeadByDepartmentId(department.getDepartmentId()));
            }
            company.setDepartmentList(departmentList);
            person.setCompany(company);
        } else if(person.getRole().getRole().contains("ROLE_HEAD_STATION")) {
            Department department = hibernateDao.getDepartmentByHeadId(person.getPersonId());
            List<Department> departmentList = new ArrayList<>();
            departmentList.add(department);
            Company company = hibernateDao.getCompanyByDepartmentId(department.getDepartmentId());
            company.setDepartmentList(departmentList);
            person.setCompany(company);
        } else {
            return "false";
        }
        module.addSerializer(Person.class, new StationSerialize());
        mapper.registerModule(module);
        String json = mapper.writeValueAsString(person);
        return json;
    }

    @RequestMapping(value = "/create-station", method = RequestMethod.POST,produces = MediaType.APPLICATION_JSON_UTF8_VALUE, consumes=MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public String createStation(@RequestBody String jsonString) throws JsonProcessingException, IOException {
        ObjectMapper mapper = new ObjectMapper();
        SimpleModule module = new SimpleModule();
        module.addDeserializer(Department.class, new DepartmentCreationDeserialize());
        mapper.registerModule(module);
        Department department = mapper.readValue(jsonString, Department.class);
        Person person = hibernateDao.getPersonById(department.getHead().getPersonId());
        department.setHead(person);
        hibernateDao.createDepartment(department);
        department = hibernateDao.getDepartmentByHeadId(department.getHead().getPersonId());
        person.setDepartmentId(department);
        Role role = person.getRole();
        if(role.getRole().contains(", ROLE_WORKER")){
            role.setRole(role.getRole().replaceAll(", ROLE_WORKER",""));
        }
        if(!role.getRole().contains("ROLE_HEAD_STATION") && !role.getRole().contains("ROLE_HEAD") &&
                !role.getRole().contains("ROLE_DIRECTOR_STATION") && !role.getRole().contains("ROLE_DIRECTOR")) {
            role.setRole(role.getRole() + ", ROLE_HEAD_STATION");
        }
        hibernateDao.updateRole(role);
        hibernateDao.mergePerson(person);
        return "true";
    }

    @RequestMapping(value = "/update-station", method = RequestMethod.POST,produces = MediaType.APPLICATION_JSON_UTF8_VALUE, consumes=MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public String updateDepartment(@RequestBody String jsonString) throws JsonProcessingException, IOException {
        ObjectMapper mapper = new ObjectMapper();
        SimpleModule module = new SimpleModule();
        module.addDeserializer(Department.class, new DepartmentUpdateDeserialize());
        mapper.registerModule(module);
        Department department = mapper.readValue(jsonString, Department.class);
        String departmentName = department.getName();
        Person person = hibernateDao.getPersonById(department.getHead().getPersonId());
        department = hibernateDao.getDepartmentById(department.getDepartmentId());
        department.setName(departmentName);
        Person oldHead = hibernateDao.getHeadByDepartmentId(department.getDepartmentId());
        person.setDepartmentId(department);
        Role role = person.getRole();
        if(role.getRole().contains(", ROLE_WORKER")){
            role.setRole(role.getRole().replaceAll(", ROLE_WORKER",""));
        }
        if(!role.getRole().contains("ROLE_HEAD_STATION") && !role.getRole().contains("ROLE_HEAD") &&
                !role.getRole().contains("ROLE_DIRECTOR_STATION") && !role.getRole().contains("ROLE_DIRECTOR")) {
            role.setRole(role.getRole() + ", ROLE_HEAD_STATION");
        }
        hibernateDao.updateRole(role);
        if(oldHead.getPersonId() != person.getPersonId()) department.setHead(person);
        hibernateDao.updateDepartment(department);
        if(oldHead.getPersonId() == person.getPersonId()) return "true";
        oldHead.setDepartmentId(null);
        role = oldHead.getRole();
        role.setRole(role.getRole().replaceAll(", ROLE_HEAD_STATION",""));
        hibernateDao.updateRole(role);
        hibernateDao.mergePerson(oldHead);

        return "true";
    }

    @RequestMapping(value = "/delete-station", method = RequestMethod.POST,produces = MediaType.APPLICATION_JSON_UTF8_VALUE, consumes=MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public String deleteDepartment(@RequestBody String jsonString) throws JsonProcessingException, IOException {
        ObjectMapper mapper = new ObjectMapper();
        SimpleModule module = new SimpleModule();
        module.addDeserializer(Department.class, new DepartmentDeleteDeserialize());
        mapper.registerModule(module);
        Department department = mapper.readValue(jsonString, Department.class);
        department = hibernateDao.getDepartmentById(department.getDepartmentId());
        Person head = hibernateDao.getHeadByDepartmentId(department.getDepartmentId());
        List<Person> personList = hibernateDao.getPersonListByDepartmentId(head.getPersonId(),department.getDepartmentId());
        for (Person person: personList) {
            person.setDepartmentId(null);
            Role role = person.getRole();
            role.setRole(role.getRole().replaceAll(", ROLE_WORKER",""));
            hibernateDao.updateRole(role);
            hibernateDao.mergePerson(person);
        }
        head.setDepartmentId(null);
        Role role = head.getRole();
        role.setRole(role.getRole().replaceAll(", ROLE_HEAD_STATION",""));
        hibernateDao.updateRole(role);
        hibernateDao.mergePerson(head);

        hibernateDao.deleteDepartment(department);
        return "true";
    }

    @RequestMapping(value = "/getgases", method = RequestMethod.POST,produces = MediaType.APPLICATION_JSON_UTF8_VALUE, consumes=MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public String getGases(@RequestBody String jsonString) throws JsonProcessingException, IOException {
        ObjectMapper mapper = new ObjectMapper();
        SimpleModule module = new SimpleModule();
        module.addDeserializer(Gas.class, new GasDeserialize());
        mapper.registerModule(module);
        Gas gas = mapper.readValue(jsonString, Gas.class);
        List<Gas> gasList = hibernateDao.getGasesByDepartmentId(gas.getGasId());
        module.addSerializer((Class)List.class, new GasesSerialize());
        mapper.registerModule(module);
        String json = mapper.writeValueAsString(gasList);
        return json;
    }

    @RequestMapping(value = "/addgas", method = RequestMethod.POST,produces = MediaType.APPLICATION_JSON_UTF8_VALUE, consumes=MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public String addGas(@RequestBody String jsonString) throws JsonProcessingException, IOException {
        ObjectMapper mapper = new ObjectMapper();
        SimpleModule module = new SimpleModule();
        module.addDeserializer(Gas.class, new AddGasDeserialize());
        mapper.registerModule(module);
        Gas gas = mapper.readValue(jsonString, Gas.class);
        hibernateDao.addGas(gas);
        return "true";
    }

    @RequestMapping(value = "/updategas", method = RequestMethod.POST,produces = MediaType.APPLICATION_JSON_UTF8_VALUE, consumes=MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public String updateGas(@RequestBody String jsonString) throws JsonProcessingException, IOException {
        ObjectMapper mapper = new ObjectMapper();
        SimpleModule module = new SimpleModule();
        module.addDeserializer(Gas.class, new UpdateGasDeserialize());
        mapper.registerModule(module);
        Gas gas = mapper.readValue(jsonString, Gas.class);
        long price = gas.getPrice();
        gas = hibernateDao.getGasById(gas.getGasId());
        gas.setPrice(price);
        hibernateDao.addGas(gas);
        return "true";
    }

    @RequestMapping(value = "/deletegas", method = RequestMethod.POST,produces = MediaType.APPLICATION_JSON_UTF8_VALUE, consumes=MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public String deleteGas(@RequestBody String jsonString) throws JsonProcessingException, IOException {
        ObjectMapper mapper = new ObjectMapper();
        SimpleModule module = new SimpleModule();
        module.addDeserializer(Gas.class, new DeleteGasDeserialize());
        mapper.registerModule(module);
        Gas gas = mapper.readValue(jsonString, Gas.class);
        gas = hibernateDao.getGasById(gas.getGasId());
        hibernateDao.deleteGas(gas);
        return "true";
    }

    @RequestMapping(value = "/cashier-registration", method = RequestMethod.POST,produces = MediaType.APPLICATION_JSON_UTF8_VALUE, consumes=MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public String cashierRegistration(@RequestBody String jsonString) throws JsonProcessingException, IOException, ParseException {
        ObjectMapper mapper = new ObjectMapper();
        SimpleModule module = new SimpleModule();
        module.addDeserializer(Person.class, new CashierRegistrationDeserialize());
        mapper.registerModule(module);
        Person person = mapper.readValue(jsonString, Person.class);
        BCryptPasswordEncoder passwordEncoder = new BCryptPasswordEncoder();
        person.setPassword(passwordEncoder.encode(person.getPassword()));
        Role role = new Role("ROLE_CASHIER", person);
        person.setRole(role);
        person.setPhone("87777777777");
        person.setEmail("email@gmail.com");
        person.setName(person.getLogin());
        person.setSurname(person.getLogin());
        Date date = new Date();
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        person.setBirthdate(dateFormat.parse(dateFormat.format(date)));
        Account account = new Account("Personal",person);
        person.setActiveAccount(account);
        try {
            hibernateDao.createRole(role);
        } catch (Exception e){
            return "false";
        }
        return "true";
    }

    @RequestMapping(value = "/cashier-update", method = RequestMethod.POST,produces = MediaType.APPLICATION_JSON_UTF8_VALUE, consumes=MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public String cashierUpdate(@RequestBody String jsonString) throws JsonProcessingException, IOException, ParseException {
        ObjectMapper mapper = new ObjectMapper();
        SimpleModule module = new SimpleModule();
        module.addDeserializer(Person.class, new CashierUpdateDeserialize());
        mapper.registerModule(module);
        Person person = mapper.readValue(jsonString, Person.class);
        String login = person.getLogin();
        person = hibernateDao.getPersonById(person.getDepartmentId().getDepartmentId());
        person.setLogin(login);
        try {
            hibernateDao.mergePerson(person);
        } catch (Exception e){
            return "false";
        }
        return "true";
    }

    @RequestMapping(value = "/getcashiers", method = RequestMethod.POST,produces = MediaType.APPLICATION_JSON_UTF8_VALUE, consumes=MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public String getCashiers(@RequestBody String jsonString) throws JsonProcessingException, IOException, ParseException {
        ObjectMapper mapper = new ObjectMapper();
        SimpleModule module = new SimpleModule();
        module.addDeserializer(Gas.class, new GasDeserialize());
        mapper.registerModule(module);
        Gas gas = mapper.readValue(jsonString, Gas.class);
        List<Person> list = hibernateDao.getCashierByDepartmentId(gas.getGasId());
        module.addSerializer((Class)List.class, new PersonSearchSerialize());
        mapper.registerModule(module);
        String json = mapper.writeValueAsString(list);
        return json;
    }

    @RequestMapping(value = "/getstation", method = RequestMethod.POST,produces = MediaType.APPLICATION_JSON_UTF8_VALUE, consumes=MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public String getDepartment(@RequestBody String jsonString) throws JsonProcessingException, IOException, ParseException {
        ObjectMapper mapper = new ObjectMapper();
        SimpleModule module = new SimpleModule();
        module.addDeserializer(Shift.class, new DepartmentShiftsDeserialize());
        mapper.registerModule(module);
        Shift shift = mapper.readValue(jsonString, Shift.class);
        Date startDate = shift.getStart();
        Date endDate = shift.getEnd();
        long page = shift.getProfit();
        List<Shift> list = hibernateDao.getShiftsByDate(shift.getShiftId(), startDate, endDate, page);
        for (Shift shift1: list) {
            Long profit = hibernateDao.getCountPrice(shift1.getShiftId());
            if(profit != null)
                shift1.setProfit(profit); else
                shift1.setProfit(new Long(0));
        }
        if(list != null){
            Shift shift2 = new Shift();
            shift2.setProfit(hibernateDao.getCountOfShifts(shift.getShiftId(), startDate, endDate));
            list.add(shift2);
        }
        module.addSerializer((Class)List.class, new ShiftSerialize());
        mapper.registerModule(module);
        String json = mapper.writeValueAsString(list);
        return json;
    }



    @RequestMapping(value = "/searchstationhead", method = RequestMethod.POST,produces = MediaType.APPLICATION_JSON_UTF8_VALUE, consumes=MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public String searchHead(@RequestBody String jsonString) throws JsonProcessingException, IOException {
        ObjectMapper mapper = new ObjectMapper();
        SimpleModule module = new SimpleModule();
        module.addDeserializer(Person.class, new CooperatorSearchDeserialize());
        mapper.registerModule(module);
        Person person = mapper.readValue(jsonString, Person.class);
        StringTokenizer st = new StringTokenizer(person.getLogin()," ");
        String pattern = st.nextToken();
        List<Person> personList;
        if(st.hasMoreTokens()){
            String pattern02 = st.nextToken();
            personList = hibernateDao.searchStationHeadByNameAndSurname(person.getDepartmentId().getDepartmentId(),pattern,pattern02);
        } else if(pattern.contains("@")){
            personList = hibernateDao.searchStationHeadByMail(person.getDepartmentId().getDepartmentId(),pattern);
        } else if(pattern.charAt(0) == '+' || pattern.charAt(0) == '8' || pattern.charAt(0) == '7') {
            personList = hibernateDao.searchStationHeadByPhone(person.getDepartmentId().getDepartmentId(),pattern);
        } else {
            personList = hibernateDao.searchStationHeadByPattern(person.getDepartmentId().getDepartmentId(),pattern);
        }
        module.addSerializer((Class)List.class, new PersonSearchSerialize());
        mapper.registerModule(module);
        String json = mapper.writeValueAsString(personList);
        return json;
    }

    @RequestMapping(value = "/stationsgasesinfo", method = RequestMethod.POST,produces = MediaType.APPLICATION_JSON_UTF8_VALUE, consumes=MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public String getStationsGasesInfo(@RequestBody String jsonString) throws JsonProcessingException {
        ObjectMapper mapper = new ObjectMapper();
        String json = "false";
        try {
            ObjectNode node = mapper.readValue(jsonString,ObjectNode.class);
            JsonNode parent = node.get("startDate");
            String startDate = parent.asText();
            parent = node.get("endDate");
            String endDate = parent.asText();
            List<Department> departmentList = hibernateDao.getStationList();
            List<TransactionHistory> transactionHistories = hibernateDao.getTransactionsByDate(startDate,endDate);
            List<StationGases> stationGases = new ArrayList<>();
            for (Department department: departmentList) {
                Map<String, Double> integerMap = new HashMap<>();
                for (TransactionHistory history: transactionHistories) {
                    if(integerMap.containsKey(history.getGas())){
                        integerMap.put(history.getGas(), integerMap.get(history.getGas()) + history.getPrice());
                    } else {
                        integerMap.put(history.getGas(), history.getPrice());
                    }
                }
                stationGases.add(new StationGases(department, integerMap));
            }

            json = mapper.writeValueAsString(stationGases);
        } catch (IOException e) {
            e.printStackTrace();
        }

        return json;
    }

    @RequestMapping(value = "/stationgasesinfo", method = RequestMethod.POST,produces = MediaType.APPLICATION_JSON_UTF8_VALUE, consumes=MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public String getStationGasesInfo(@RequestBody String jsonString) throws JsonProcessingException {
        ObjectMapper mapper = new ObjectMapper();
        SimpleModule module = new SimpleModule();
        String json = "false";
        try {
            module.addDeserializer(Person.class, new DepartmentShiftsDeserialize());
            mapper.registerModule(module);
            Shift shift = mapper.readValue(jsonString, Shift.class);
            Department department = hibernateDao.getDepartmentById(shift.getShiftId());
            List<TransactionHistory> transactionHistories = hibernateDao.getStationTransactionsByDate(shift.getStart(),shift.getEnd(),shift.getProfit());
            Map<String, Double> integerMap = new HashMap<>();
            for (TransactionHistory history: transactionHistories) {
                if(integerMap.containsKey(history.getGas())){
                    integerMap.put(history.getGas(), integerMap.get(history.getGas()) + history.getPrice());
                } else {
                    integerMap.put(history.getGas(), history.getPrice());
                }
            }
            StationGases stationGases = new StationGases(department, integerMap);

            json = mapper.writeValueAsString(stationGases);
        } catch (IOException e) {
            e.printStackTrace();
        }

        return json;
    }
}
