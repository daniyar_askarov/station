package kz.gasstation.spring.controller;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.module.SimpleModule;
import kz.gasstation.spring.dao.deserialize.DashboardDeserialize;
import kz.gasstation.spring.dao.entities.Account;
import kz.gasstation.spring.dao.entities.Person;
import kz.gasstation.spring.dao.entities.Role;
import kz.gasstation.spring.dao.entities.TransactionHistory;
import kz.gasstation.spring.dao.implementations.HibernateDao;
import kz.gasstation.spring.dao.serialize.DashboardInfoSerialize;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;


import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

@Controller
public class HomeController {

    @Autowired
    HibernateDao hibernateDao;

    @GetMapping(value = "/")
    public ModelAndView login(){
        return new ModelAndView("index");
    }

    @GetMapping(value = "/cashier")
    public ModelAndView cashier(){
        return new ModelAndView("index");
    }

    @RequestMapping(value = "/dashboard", method = RequestMethod.POST,produces = MediaType.APPLICATION_JSON_UTF8_VALUE, consumes=MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public String getPersonByLoginAndPassword(@RequestBody String jsonString) throws JsonProcessingException, ParseException {
        ObjectMapper mapper = new ObjectMapper();
        SimpleModule module = new SimpleModule();
        Person person;
        String json;
        try {
            module.addDeserializer(Person.class, new DashboardDeserialize());
            mapper.registerModule(module);
            person = mapper.readValue(jsonString, Person.class);
            String password = person.getPassword();
            person = hibernateDao.getPersonByLoginAndPassword(person.getLogin(), password);
            if(person==null || (person.getRole().getRole().contains("ROLE_CASHIER")) ||
                    (person.getRole().getRole().contains("ROLE_HEAD_STATION")) ||
                    (person.getRole().getRole().contains("ROLE_DIRECTOR_STATION"))) return "false";
            person.setPassword(password);
            Role role = hibernateDao.getRoleByPersonId(person.getPersonId());
            person.setRole(role);
            SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
            Calendar cal = Calendar.getInstance();
            Date endDate = dateFormat.parse(dateFormat.format(cal.getTime()));
            cal.add(Calendar.MONTH, -1);
            Date startDate = dateFormat.parse(dateFormat.format(cal.getTime()));
            List<TransactionHistory> list = hibernateDao.getTransactionsByPersonId(person.getPersonId(),1,startDate,endDate);
            person.setTransactionHistories(list);
            List<Account> accountList = hibernateDao.getAccountListByPersonId(person.getPersonId());
            person.setAccounts(accountList);
            module.addSerializer(Person.class, new DashboardInfoSerialize());
            mapper.registerModule(module);
            json = mapper.writeValueAsString(person);
            return json;
        } catch (IOException e) {
            e.printStackTrace();
        }
        return null;
    }
}