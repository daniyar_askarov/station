package kz.gasstation.spring.controller;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.module.SimpleModule;
import kz.gasstation.spring.dao.deserialize.*;
import kz.gasstation.spring.dao.entities.Account;
import kz.gasstation.spring.dao.entities.Person;
import kz.gasstation.spring.dao.entities.Role;
import kz.gasstation.spring.dao.implementations.HibernateDao;
import kz.gasstation.spring.dao.serialize.PersonSerialize;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;

import java.io.IOException;

@RestController
public class PersonController {

    @Autowired
    HibernateDao hibernateDao;

    @RequestMapping(value = "/getperson", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_UTF8_VALUE, consumes=MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public String getPerson(@RequestBody String jsonString) throws IOException, JsonProcessingException {
        ObjectMapper mapper = new ObjectMapper();
        SimpleModule module = new SimpleModule();
        module.addDeserializer(Person.class, new GetPersonDeserialize());
        mapper.registerModule(module);
        Person person = mapper.readValue(jsonString, Person.class);
        String password = person.getPassword();
        person = hibernateDao.getPersonByLoginAndPassword(person.getLogin(),password);
        if(person==null && person.getRole().getRole().contains("ROLE_CASHIER")) return "false";
        person.setPassword(password);
        module.addSerializer(Person.class, new PersonSerialize());
        mapper.registerModule(module);
        String json = mapper.writeValueAsString(person);
        return json;
    }

    @RequestMapping(value = "/mobileregistration", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_UTF8_VALUE, consumes=MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public String mobileRegistration(@RequestBody String jsonString) throws IOException, JsonProcessingException {
        ObjectMapper mapper = new ObjectMapper();
        SimpleModule module = new SimpleModule();
        module.addDeserializer(Person.class, new RegistrationDeserialize());
        mapper.registerModule(module);
        Person person = mapper.readValue(jsonString, Person.class);
        String password =person.getPassword();
        Role role = new Role("ROLE_USER",person);
        person.setRole(role);
        Account account = new Account("Personal",person);
        person.setActiveAccount(account);
        BCryptPasswordEncoder passwordEncoder = new BCryptPasswordEncoder();
        person.setPassword(passwordEncoder.encode(person.getPassword()));
        hibernateDao.createRole(role);
        person = hibernateDao.getPersonByLoginAndPassword(person.getLogin(),password);
        person.setPassword(password);
        module.addSerializer(Person.class, new PersonSerialize());
        mapper.registerModule(module);
        String json = mapper.writeValueAsString(person);
        return json;
    }

    @RequestMapping(value = "/registration", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_UTF8_VALUE, consumes=MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public String registration(@RequestBody String jsonString) throws IOException, JsonProcessingException {
        ObjectMapper mapper = new ObjectMapper();
        SimpleModule module = new SimpleModule();
        module.addDeserializer(Person.class, new RegistrationDeserialize());
        mapper.registerModule(module);
        Person person = mapper.readValue(jsonString, Person.class);
        Role role = new Role("ROLE_USER",person);
        person.setRole(role);
        Account account = new Account("Personal",person);
        person.setActiveAccount(account);
        BCryptPasswordEncoder passwordEncoder = new BCryptPasswordEncoder();
        person.setPassword(passwordEncoder.encode(person.getPassword()));
        try {
            hibernateDao.createRole(role);
        } catch (Exception e){
            return "false";
        }
        return "true";
    }

    @RequestMapping(value = "/update-person", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_UTF8_VALUE, consumes=MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public String updatePerson(@RequestBody String jsonString) throws JsonProcessingException {
        ObjectMapper mapper = new ObjectMapper();
        SimpleModule module = new SimpleModule();
        Person person1;
        try {
            module.addDeserializer(Person.class, new PersonDeserialize());
            mapper.registerModule(module);
            person1 = mapper.readValue(jsonString, Person.class);
            Person person = hibernateDao.getPersonById(person1.getPersonId());
            person.setName(person1.getName());
            person.setSurname(person1.getSurname());
            person.setPassword(person1.getPassword());
            person.setEmail(person1.getEmail());
            person.setPhone(person1.getPhone());
            hibernateDao.updatePerson(person);
        }
        catch (Exception e) {
            return "false";
        }
        return "true";
    }

    @RequestMapping(value = "/updateperson", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_UTF8_VALUE, consumes=MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public String personUpdate(@RequestBody String jsonString) throws JsonProcessingException {
        ObjectMapper mapper = new ObjectMapper();
        SimpleModule module = new SimpleModule();
        Person person1;
        try {
            module.addDeserializer(Person.class, new PersonUpdateDeserialize());
            mapper.registerModule(module);
            person1 = mapper.readValue(jsonString, Person.class);
            Person person = hibernateDao.getPersonById(person1.getPersonId());
            person.setName(person1.getName());
            person.setSurname(person1.getSurname());
            person.setPassword(person1.getPassword());
            person.setEmail(person1.getEmail());
            person.setPhone(person1.getPhone());
            person.setBirthdate(person1.getBirthdate());
            hibernateDao.updatePerson(person);
        }
        catch (Exception e) {
            return "false";
        }
        return "true";
    }





}
