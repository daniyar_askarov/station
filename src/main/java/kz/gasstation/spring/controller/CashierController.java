package kz.gasstation.spring.controller;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.module.SimpleModule;
import kz.gasstation.spring.dao.deserialize.*;
import kz.gasstation.spring.dao.entities.*;
import kz.gasstation.spring.dao.implementations.HibernateDao;
import kz.gasstation.spring.dao.serialize.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;

import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

@RestController
public class CashierController {

    @Autowired
    HibernateDao hibernateDao;

    @RequestMapping(value = "/user-payment", method = RequestMethod.POST,produces = MediaType.APPLICATION_JSON_UTF8_VALUE, consumes=MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public String userpayment(@RequestBody String jsonString) throws JsonProcessingException {
        ObjectMapper mapper = new ObjectMapper();

        try {
            PaymentWait payment = mapper.readValue(jsonString, PaymentWait.class);
            hibernateDao.addUserPayment(payment);
        } catch (IOException e) {
            e.printStackTrace();
        }

        return "true";
    }


    @RequestMapping(value = "/getUserPayments", method = RequestMethod.POST,produces = MediaType.APPLICATION_JSON_UTF8_VALUE, consumes=MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public String getUserPayments(@RequestBody String jsonString) throws JsonProcessingException {
        ObjectMapper mapper = new ObjectMapper();
        String json = "error";
        try {
            PaymentWait payment = mapper.readValue(jsonString, PaymentWait.class);
            List<PaymentWait> paymentWaitList = hibernateDao.getPaymentHistory(payment.getDepartmentId());
            json = mapper.writeValueAsString(paymentWaitList);
        } catch (IOException e) {
            e.printStackTrace();
        }

        return json;
    }

    @RequestMapping(value = "/deleteUserPayment", method = RequestMethod.POST,produces = MediaType.APPLICATION_JSON_UTF8_VALUE, consumes=MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public String deleteUserPayment(@RequestBody String jsonString) throws JsonProcessingException {
        ObjectMapper mapper = new ObjectMapper();
        String json = "false";
        try {
            PaymentWait payment = mapper.readValue(jsonString, PaymentWait.class);
            hibernateDao.deletePayment(payment);
            json = "true";
        } catch (IOException e) {
            e.printStackTrace();
        }

        return json;
    }

    @RequestMapping(value = "/getShiftId", method = RequestMethod.POST,produces = MediaType.APPLICATION_JSON_UTF8_VALUE, consumes=MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public String getShiftId(@RequestAttribute("departmentId") String departmentId){
        Shift shift = hibernateDao.getShiftId(departmentId);
        String json = String.valueOf(shift.getShiftId());
        return json;
    }

    @RequestMapping(value = "/refund", method = RequestMethod.POST,produces = MediaType.APPLICATION_JSON_UTF8_VALUE, consumes=MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public String refund(@RequestBody String jsonString) throws JsonProcessingException {
        ObjectMapper mapper = new ObjectMapper();
        String json = "false";
        try {
            TransactionHistory transactionHistory = mapper.readValue(jsonString, TransactionHistory.class);
            transactionHistory = hibernateDao.getTransactionsById(transactionHistory.getTransactionId());
            Person person = hibernateDao.getPersonByLogin(transactionHistory.getLogin());
            Account account = hibernateDao.getActiveAccount(person.getPersonId(), transactionHistory.getBalance());
            account.setBalance(account.getBalance() + transactionHistory.getPrice());
            hibernateDao.updateAccountBalance(account);
            transactionHistory.setRefund(true);
            hibernateDao.updateTransaction(transactionHistory);
            json = "true";
        } catch (IOException e) {
            e.printStackTrace();
        }

        return json;
    }

    @RequestMapping(value = "/cashier-payment", method = RequestMethod.POST,produces = MediaType.APPLICATION_JSON_UTF8_VALUE, consumes=MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public String getpayment(@RequestBody String jsonString) throws JsonProcessingException {
        ObjectMapper mapper = new ObjectMapper();
        SimpleModule module = new SimpleModule();
        TransactionHistory transactionHistory;
        try {
            module.addDeserializer(TransactionHistory.class, new PaymentDeserialize());
            mapper.registerModule(module);
            transactionHistory = mapper.readValue(jsonString, TransactionHistory.class);
            Person person = hibernateDao.getPersonById(transactionHistory.getPersonId().getPersonId());
            Account activeAccount = hibernateDao.getActiveAccount(person.getPersonId(), transactionHistory.getPersonId().getActiveAccount().getName());
            Shift shift = hibernateDao.getShiftById(transactionHistory.getShiftId().getShiftId());
            if(shift.getPersonId().getDepartment()==null) return "The cashier is outside the department";
            double price = transactionHistory.getPrice();
            double balance = activeAccount.getBalance();
            if (price > balance) return "false";
            activeAccount.setBalance(balance - price);
            hibernateDao.updateAccountBalance(activeAccount);

            Date date = new Date();
            SimpleDateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");

            person.setActiveAccount(activeAccount);
            transactionHistory.setShiftId(shift);
            transactionHistory.setPersonId(person);
            transactionHistory.setDate(dateFormat.parse(dateFormat.format(date)));
            transactionHistory.setBalance(person.getActiveAccount().getName());
            transactionHistory.setDepartmentName(shift.getPersonId().getDepartment().getName());
            transactionHistory.setLogin(person.getLogin());

            hibernateDao.addTransaction(new TransactionHistory(transactionHistory.getPrice(),transactionHistory.getPriceLiter(),
                    transactionHistory.getDate(), transactionHistory.getBalance(),
                    transactionHistory.getPersonId(),transactionHistory.getGas(), transactionHistory.getLogin(),
                    transactionHistory.getShiftId(),transactionHistory.getDepartmentName()));

        } catch (IOException e) {
            e.printStackTrace();
        } catch (ParseException e) {
            e.printStackTrace();
        }

        return "true";
    }

    @RequestMapping(value = "/cashierauth", method = RequestMethod.POST,produces = MediaType.APPLICATION_JSON_UTF8_VALUE, consumes=MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public String cashierAuthorization(@RequestBody String jsonString) throws JsonProcessingException,IOException,ParseException {
        ObjectMapper mapper = new ObjectMapper();
        SimpleModule module = new SimpleModule();
        module.addDeserializer(Person.class, new GetPersonDeserialize());
        mapper.registerModule(module);
        Person person = mapper.readValue(jsonString, Person.class);
        String password = person.getPassword();
        person = hibernateDao.getPersonByLoginAndPassword(person.getLogin(),password);
        if(person==null||(!person.getRole().getRole().contains("ROLE_CASHIER"))) return "false";

        List<Shift> shiftList = hibernateDao.getOpenShiftsByPersonId(person.getPersonId());
        person.setShifts(shiftList);
        Department department = hibernateDao.getDepartmentById(person.getDepartmentId().getDepartmentId());
        List<Gas> gases = hibernateDao.getGasesByDepartmentId(department.getDepartmentId());
        department.setGases(gases);
        person.setDep(department);

        module.addSerializer(Person.class, new CashierAuthSerialize());
        mapper.registerModule(module);
        String json = mapper.writeValueAsString(person);
        return json;
    }

    @RequestMapping(value = "/startshift", method = RequestMethod.POST,produces = MediaType.APPLICATION_JSON_UTF8_VALUE, consumes=MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public String startshift(@RequestBody String jsonString) throws JsonProcessingException,IOException,ParseException {
        ObjectMapper mapper = new ObjectMapper();
        SimpleModule module = new SimpleModule();
        module.addDeserializer(Person.class, new B2BHomeDeserialize());
        mapper.registerModule(module);
        Person person = mapper.readValue(jsonString, Person.class);

        Date date = new Date();
        SimpleDateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
        Shift shift = new Shift();
        shift.setStart(dateFormat.parse(dateFormat.format(date)));
        shift.setStatus("open");
        shift.setPersonId(person);
        hibernateDao.createShift(shift);

        shift = hibernateDao.getOpenShiftsByPersonId(person.getPersonId()).get(0);
        module.addSerializer(Shift.class, new ShiftStartSerialize());
        mapper.registerModule(module);
        String json = mapper.writeValueAsString(shift);
        return json;
    }

    @RequestMapping(value = "/cashierlogout", method = RequestMethod.POST,produces = MediaType.APPLICATION_JSON_UTF8_VALUE, consumes=MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public String cashierLogout(@RequestBody String jsonString) throws JsonProcessingException,IOException,ParseException {
        ObjectMapper mapper = new ObjectMapper();
        SimpleModule module = new SimpleModule();
        module.addDeserializer(Person.class, new ShiftDeserialize());
        mapper.registerModule(module);
        Shift shift = mapper.readValue(jsonString, Shift.class);
        shift = hibernateDao.getShiftById(shift.getShiftId());
        if(shift.getEnd()!= null) return "false";
        Date date = new Date();
        SimpleDateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
        shift.setEnd(dateFormat.parse(dateFormat.format(date)));
        shift.setStatus("close");
        List<TransactionHistory> list = hibernateDao.getTransactionsByShiftId(shift.getShiftId());long sum = 0;
        for (TransactionHistory transaction: list) {
            sum += transaction.getPrice();
        }
        shift.setProfit(sum);
        hibernateDao.updateShift(shift);
        shift.setTransactions(list);
        module.addSerializer(Shift.class, new ShiftTransactionsSerialize());
        mapper.registerModule(module);
        String json = mapper.writeValueAsString(shift);
        return json;
    }


    @RequestMapping(value = "/openshifts", method = RequestMethod.POST,produces = MediaType.APPLICATION_JSON_UTF8_VALUE, consumes=MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public String getOpenShift(@RequestBody String jsonString) throws JsonProcessingException,IOException,ParseException {
        ObjectMapper mapper = new ObjectMapper();
        SimpleModule module = new SimpleModule();
        module.addDeserializer(Shift.class, new DepartmentShiftsDeserialize());
        mapper.registerModule(module);
        Shift shift = mapper.readValue(jsonString, Shift.class);
        List<Shift> shiftList = hibernateDao.getOpenShiftsByDate(shift.getShiftId(),shift.getStart(),shift.getEnd());
        module.addSerializer((Class)List.class, new ShiftSerialize());
        mapper.registerModule(module);
        String json = mapper.writeValueAsString(shiftList);
        return json;
    }

    @RequestMapping(value = "/closeshifts", method = RequestMethod.POST,produces = MediaType.APPLICATION_JSON_UTF8_VALUE, consumes=MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public String getCloseShift(@RequestBody String jsonString) throws JsonProcessingException,IOException,ParseException {
        ObjectMapper mapper = new ObjectMapper();
        SimpleModule module = new SimpleModule();
        module.addDeserializer(Shift.class, new DepartmentShiftsDeserialize());
        mapper.registerModule(module);
        Shift shift = mapper.readValue(jsonString, Shift.class);
        List<Shift> shiftList = hibernateDao.getCloseShiftsByDate(shift.getShiftId(),shift.getStart(),shift.getEnd());
        module.addSerializer((Class)List.class, new ShiftSerialize());
        mapper.registerModule(module);
        String json = mapper.writeValueAsString(shiftList);
        return json;
    }

    @RequestMapping(value = "/personshifts", method = RequestMethod.POST,produces = MediaType.APPLICATION_JSON_UTF8_VALUE, consumes=MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public String getPersonalShift(@RequestBody String jsonString) throws JsonProcessingException,IOException,ParseException {
        ObjectMapper mapper = new ObjectMapper();
        SimpleModule module = new SimpleModule();
        module.addDeserializer(Shift.class, new PersonShiftsDeserialize());
        mapper.registerModule(module);
        Shift shift = mapper.readValue(jsonString, Shift.class);
        List<Shift> shiftList = hibernateDao.getPersonShiftsByDate(shift.getShiftId(),shift.getStart(),shift.getEnd());
        module.addSerializer((Class)List.class, new ShiftSerialize());
        mapper.registerModule(module);
        String json = mapper.writeValueAsString(shiftList);
        return json;
    }

    @RequestMapping(value = "/shifttransactions", method = RequestMethod.POST,produces = MediaType.APPLICATION_JSON_UTF8_VALUE, consumes=MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public String getShiftTransactions(@RequestBody String jsonString) throws JsonProcessingException,IOException,ParseException {
        ObjectMapper mapper = new ObjectMapper();
        SimpleModule module = new SimpleModule();
        module.addDeserializer(Person.class, new ShiftDeserialize());
        mapper.registerModule(module);
        Shift shift = mapper.readValue(jsonString, Shift.class);
        shift = hibernateDao.getShiftById(shift.getShiftId());
        List<TransactionHistory> list = hibernateDao.getTransactionsByShiftId(shift.getShiftId());
        long sum = 0;
        for (TransactionHistory transaction: list) {
            sum += transaction.getPrice();
        }
        shift.setProfit(sum);
        Person person = shift.getPersonId();
        person.setDep(hibernateDao.getDepartmentById(person.getDepartmentId().getDepartmentId()));
        shift.setPersonId(person);
        shift.setTransactions(list);
        module.addSerializer(Shift.class, new ShiftTransactionsSerialize());
        mapper.registerModule(module);
        String json = mapper.writeValueAsString(shift);
        return json;
    }

    @RequestMapping(value = "/companyprofit", method = RequestMethod.POST,produces = MediaType.APPLICATION_JSON_UTF8_VALUE, consumes=MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public String getCompanyProfit(@RequestBody String jsonString) throws JsonProcessingException,IOException,ParseException {
        ObjectMapper mapper = new ObjectMapper();
        SimpleModule module = new SimpleModule();
        module.addDeserializer(Shift.class, new CompanyDeserialize());
        mapper.registerModule(module);
        Shift shift = mapper.readValue(jsonString, Shift.class);
        List<Company> companies = hibernateDao.searchCompanyByDep(shift.getShiftId());
        for (Company company: companies) {
            Person head = new Person(hibernateDao.getSumPrice(company.getCompanyId(),shift.getStart(),shift.getEnd()));
            head.setDepartment(new Department(hibernateDao.getSumLiters(company.getCompanyId(),shift.getStart(),shift.getEnd())));
            company.setHead(head);
        }
        module.addSerializer((Class)List.class, new CompanyProfitSerialize());
        mapper.registerModule(module);
        String json = mapper.writeValueAsString(companies);
        return json;
    }

    @RequestMapping(value = "/personprofit", method = RequestMethod.POST,produces = MediaType.APPLICATION_JSON_UTF8_VALUE, consumes=MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public String getPersonProfit(@RequestBody String jsonString) throws JsonProcessingException,IOException,ParseException {
        ObjectMapper mapper = new ObjectMapper();
        SimpleModule module = new SimpleModule();
        module.addDeserializer(Shift.class, new PersonProfitDeserialize());
        mapper.registerModule(module);
        Shift shift = mapper.readValue(jsonString, Shift.class);
        List<Person> people = hibernateDao.getPersonListByCompanyId(shift.getShiftId());
        for (Person person: people) {
            Department department = new Department(hibernateDao.getSumPersonPrice(person.getPersonId(),shift.getStart(),shift.getEnd()));
            department.setHead(new Person(hibernateDao.getSumPersonLiters(person.getPersonId(),shift.getStart(),shift.getEnd())));
            person.setTransactionHistories(hibernateDao.getSumPersonGases(person.getPersonId(),shift.getStart(),shift.getEnd()));
            for (TransactionHistory history : person.getTransactionHistories()){
                history.setPrice(hibernateDao.getPersonProfitByGase(person.getPersonId(),history.getGas(),shift.getStart(),shift.getEnd()));
                history.setPriceLiter(hibernateDao.getPersonLitersByGase(person.getPersonId(),history.getGas(),shift.getStart(),shift.getEnd()));
            }
            person.setDepartment(department);
        }
        people.get(0).getDepartment().setGases(hibernateDao.getSumGases(shift.getShiftId(),shift.getStart(),shift.getEnd()));
        for (Gas gas: people.get(0).getDepartment().getGases()) {
            gas.setPrice(hibernateDao.getProfitByGase(shift.getShiftId(),gas.getName(),shift.getStart(),shift.getEnd()));
            gas.setGasId(hibernateDao.getLitersByGase(shift.getShiftId(),gas.getName(),shift.getStart(),shift.getEnd()));
        }
        module.addSerializer((Class)List.class, new PersonProfitSerialize());
        mapper.registerModule(module);
        String json = mapper.writeValueAsString(people);
        return json;
    }
}